const logger = require('log4js').getLogger("blogHandler");
const configurationService = require("../service/configurationService");
const blogService = require('../service/blogService');
const adminOrgId =  configurationService.get('adminOrgId');
const contentDisposition = require('content-disposition');

exports.saveBlog = saveBlog;
exports.getBlogsList = getBlogsList;
exports.getBlogByBlogId = getBlogByBlogId;
exports.getBlogImageByBlogId = getBlogImageByBlogId;
exports.deleteBlog = deleteBlog;


function saveBlog(req, res){

  let userId = req.params.user.id;
  let blogFile = req.files.blogFile;

  let blogDetails = {
    blogName : req.body.blogName,
    blogDesc : req.body.blogDesc,
    userType : req.body.userType
  }

  blogService.saveBlog({userId : userId, blogDetails : blogDetails, blogFile : blogFile})
  .then((blogDetails) => res.send(200,{success : true}))
  .catch((error) => {
    logger.warn("Error occured while creating blogs",{error : error});
    res.send(500);
  })
}

function deleteBlog(req, res){
  let blogId = req.params.blog.id;
  let userId = req.params.user.id;

  blogService.deleteBlog({ blogId : blogId, userId : userId})
  .then((blogDetails) => res.send(200,{success : true}))
  .catch((error) => {
    logger.warn("Error occured while creating blogs",{error : error});
    res.send(500);
  })
}

function getBlogsList(req, res){

  let userId = adminOrgId;
  let endIndex = req.params.blog.endIndex;

  blogService.getBlogsList({ userId : userId, endIndex : endIndex})
  .then((blogs) => res.send(200,{success : true, data : blogs}))
  .catch((error) => {
    logger.warn("Error occured while fetching blogs",{error : error});
    res.send(500);
  })
}


function getBlogByBlogId(req, res){
  let blogId  = req.params.blog.id;

  blogService.getBlogByBlogId({blogId : blogId})
  .then((blog) => res.send(200,{success : true, data : blog}))
  .catch((error) => {
    logger.warn("Error occured while fetching blog",{blogId : blogId})
    res.send(500);
  })
}


function getBlogImageByBlogId(req, res){
  let imageId = req.params.image.id;

  let contentType , fileName;
  blogService.getBlogByBlogId({blogId : imageId})
  .then((blog) => {
    contentType = blog._attachments[Object.keys(blog._attachments)[0]].content_type;
    fileName = Object.keys(blog._attachments)[0];
    return blogService.getReadStream({docId: imageId,fileName : fileName});
  })
  .then((readStream) => {
    res.setHeader('Content-disposition', 'attachment; filename=' + contentDisposition(fileName));
    res.setHeader("Content-Type", contentType);
    // pipe the attachment to the client's response
    readStream.pipe(res);
  })
  .catch((error) => {
    logger.warn("Error occured while fetching blog",{blogId : blogId})
    res.send(500);
  })
}