exports.noStore = noStore;
exports.noCache = noCache;


function noStore(req, res, next) {
  res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
  res.setHeader('Pragma', 'no-cache');
  res.setHeader('Expires', 0);
  next();
}

function noCache(req, res, next) {
  res.setHeader('Cache-Control', 'max-age=1, must-revalidate');
  next();
}