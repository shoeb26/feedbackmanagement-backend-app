const logger = require('log4js').getLogger("surveyHandler");
const configurationService = require("../service/configurationService");
const surveyService = require('../service/surveyService');

exports.addSurvey = addSurvey;
exports.updateSurvey = updateSurvey;
exports.getSurveyBySurveyId = getSurveyBySurveyId;
exports.getSurveysByUser = getSurveysByUser;
exports.updateSurveyStatus = updateSurveyStatus;
exports.getChartType = getChartType;
exports.getSurveysByVillageCode = getSurveysByVillageCode;


function addSurvey(req, res){
  logger.trace("Adding survey intiatied");
  let userId =  req.params.user.id;
  let surveyDetails = req.body.surveyDetails;
  let userDetails = req.body.userDetails;

  surveyService.createSurvey({userId : userId, surveyDetails : surveyDetails, userDetails : userDetails})
  	.then((data) => res.send(200,{success : true, survey : data.id}))
  	.catch((error) => {
      if(error.msg == "Survey limit reached"){
        return res.send(200,{success : false ,msg :"Survey limit reached"});  
      }
  		logger.warn("addSurvey - Error occured while creating survey",{error : error});
      return res.send(500);
  	})
}

function updateSurvey(req, res){
  logger.trace("staring to update survey");
  let surveyId  = req.params.survey.id;
  let surveyDetails = req.body.surveyDetails;
  let categoryId = req.body.categoryId;

  surveyService.updateSurvey({categoryId : categoryId, surveyDetails : surveyDetails, surveyId : surveyId})
    .then((data) => res.send(200,{success : true, survey : data.id}))
    .catch((error) => {
      logger.warn("addSurvey - Error occured while creating survey",{error : error});
      return res.send(500);
    })

}



function getChartType(req, res){

  let chartType = req.params.chart.type;
  let villageCode = req.params.village.code;
  let userId = req.params.user.id;

  surveyService.getChartData({ chartType : chartType, villageCode : villageCode, userId : userId})
  .then((data) => res.send(200,{success : true, data : data}))
  .catch((error) => {
    logger.warn("getChartData - Error occured while fetching chart Details",{error : error});
    return res.send(500);
  })
}

function getSurveyBySurveyId(req, res){
	logger.trace("Fetching survey details");

  let surveyId = req.params.survey.id;

  surveyService.getSurveyBySurveyId({surveyId : surveyId})
    .then((survey) =>  res.send(200,{success : true, survey : survey}))
    .catch((error) => {
      logger.warn("Error occured while fetching survey details",{surveyId : surveyId, error : error});
      return reject(error);
    })
}


function getSurveysByUser(req, res){
  let userId = req.params.user.id;

  surveyService.getSurveyListByUserId({userId : userId})
  .then((survey) =>  res.send(200,{success : true, surveys : survey}))
  .catch((error) => {
    if (error.msg=="User not present") {
      logger.warn('getSurveyListByUserId - user not found for userId',{userId : userId, error:error});
      return res.send(200,{success : false, msg :  "User not present"});
    }
    logger.warn("Error occured while fetching survey details",{userId : userId, error : error});
    return res.send(500);
  })
}

function updateSurveyStatus(req, res){
  let surveyId = req.params.survey.id;

  surveyService.updateSurveyStatus({surveyId : surveyId})
  .then((success) => res.send(200,{success : true}))
  .catch((error) => {
    logger.warn("Error occured while updating survey status",{surveyId : surveyId, error : error});
    return reject(error);
  })
}

function getSurveysByVillageCode(req, res){
  let villageCode  = req.params.surveys.villageCode;
  let userId = req.params.user.id;

  surveyService.getSurveysByVillageCode({villageCode : villageCode})
  .then((survey) =>  res.send(200,{success : true, surveys : survey}))
  .catch((error) => {
    logger.warn("Error occured while fetching survey details by village code",{userId : userId,villageCode : villageCode, error : error});
    return res.send(500);
  })
}