const logger = require('log4js').getLogger("surveyHandler");
const configurationService = require("../service/configurationService");
const categoryService = require('../service/categoryService');

exports.getCategoryByCategoryIdAndLocale = getCategoryByCategoryIdAndLocale;
exports.getCategoryListByLocale = getCategoryListByLocale;
exports.getCategoryByCategoryIdAndSurveyId = getCategoryByCategoryIdAndSurveyId;
exports.getCategoryListByLocaleAndSurvey =getCategoryListByLocaleAndSurvey;

function getCategoryListByLocale(req, res){

 let locale = req.params.category.locale;

 categoryService.getCategoryListByLocale({locale : locale})
  .then((categories) => res.send(200,{success : true, categories : categories}))
  .catch((error) => {
    logger.warn("getCategoryListByLocale() - Error occured while fetching categories");
    res.send(500);
  }) 
}


function getCategoryByCategoryIdAndLocale(req, res){

 let locale = req.params.category.locale;
 let categoryId = req.params.category.id;

 categoryService.getCategoryByCategoryIdAndLocale({locale : locale, categoryId : categoryId})
  .then((categories) => res.send(200,{success : true, category : categories}))
  .catch((error) => {
    logger.warn("getCategoryByCategoryIdAndLocale() - Error occured while fetching categories",{error : error});
    res.send(500);
  })
}


function getCategoryByCategoryIdAndSurveyId(req, res){

 let surveyId = req.params.survey.id;
 let categoryId = req.params.category.id;

 categoryService.getCategoryByCategoryIdAndSurveyId({surveyId : surveyId, categoryId : categoryId})
  .then((categories) => res.send(200,{success : true, category : categories}))
  .catch((error) => {
    logger.warn("getCategoryByCategoryIdAndLocale() - Error occured while fetching categories",{error : error});
    res.send(500);
  })
}


function getCategoryListByLocaleAndSurvey(req, res){
  let locale = req.params.category.locale;
  let surveyId = req.params.survey.id;


  categoryService.getCategoryListByLocaleAndSurvey({surveyId : surveyId, locale : locale})
  .then((categories) => res.send(200,{success : true, category : categories}))
  .catch((error) => {
    logger.warn("getCategoryByCategoryIdAndLocale() - Error occured while fetching categories",{error : error});
    res.send(500);
  })
}