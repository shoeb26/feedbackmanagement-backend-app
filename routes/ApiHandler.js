var httpAuthorizeService = require("../service/httpAuthorizeService");
var noStore = require('./CacheHandler').noStore;
var noCache = require('./CacheHandler').noCache;
var apiHandlerLogger = require('log4js').getLogger("ApiHandler");



/**
 * setup API with handlers
 * @param server
 * @param handler
 */
exports.setup = function (server, handler) {
  /**
   * READ THIS: You cannot add new root paths to the rest handler without also updating the nginx configuration.
   * Nginx needs to be told to send requests on /restapi/ to the restapi port of nodejs.  It also needs
   * to be configured to send requests on /systemUpdate/ to the rest api port as well.  If you add another
   * root path, a sibling of restapi and systemUpdate, it will not work.  All the requests will go to the UI
   * server.
   */

  server.on('NotFound', function (request, response, cb) {
    response.send(404, " rest api not found");
  });


  /****
   *
   * API's WHICH WILL NEVER REQUIRE AUTHENTICATION AND AUTHORIZATION.
   *
   */
  server.post('/restapi/activeUser', [noStore, handler.users.activeUser]);
  server.post(/\/restapi\/user:email=([^:/]*)\/session/ ,[noStore, handler.users.login]);
  server.post(/\/restapi\/user:email=([^:/]*)\/suggestions/ ,[noStore, handler.users.suggestions]);
  server.post(/\/restapi\/user:email=([^:/]*)\/forgotpassoword/ ,[noStore, handler.users.forgotPassword]);
  server.get(/\/restapi\/state:name=([^:/]*)\/districts/ ,[noStore, handler.village.getDistricts]);
  server.get(/\/restapi\/state:name=([^:/]*)\/district:name=([^:/]*)\/villages/ ,[noStore, handler.village.getVillages]);
  server.put('/restapi/user/createUser' ,[noStore, handler.users.register]);
  server.get(/\/restapi\/org:id=([^:/]*)\/image:id=([^:/]*)/, [noCache, handler.blogs.getBlogImageByBlogId]);
  server.get(/\/restapi\/org:id=([^:/]*)\/blog:id=([^:/]*)/, [noCache, handler.blogs.getBlogByBlogId]);
  server.get(/\/restapi\/org:id=([^:/]*)\/blog:endIndex=([^:/]*)/, [noCache, handler.blogs.getBlogsList]);

  server.use(httpAuthorizeService.httpAuthorizerCheck);


  /*****
   *
   * API's WHICH REQUIRE AUTHENTICATION AND AUTHORIZATION BUT NEED TO CHANGE THE API CONVENTIONS.
   * AFTER CHANGING THE API CONVENTION THE API SHOULD BE SHIFT BELOW SERVER.USE CALLING AUTHENTICATION
   * AND AUTHORIZATION FUNCTION.
   *
   */
  server.put('/restapi/org:id=([^:/]*)/user:id=([^:/]*)/survey', [noCache, handler.survey.addSurvey]);
  server.post('/restapi/org:id=([^:/]*)/user:id=([^:/]*)/survey:id=([^:/]*)', [noCache, handler.survey.updateSurvey]);
  server.get('/restapi/org:id=([^:/]*)/user:id=([^:/]*)/survey:id=([^:/]*)', [noCache, handler.survey.getSurveyBySurveyId]);
  server.get('/restapi/org:id=([^:/]*)/user:id=([^:/]*)/village:code=([^:/]*)/chart:type=([^:/]*)', [noCache, handler.survey.getChartType]);
  server.get('/restapi/org:id=([^:/]*)/user:id=([^:/]*)/surveys', [noCache, handler.survey.getSurveysByUser]);
  server.get('/restapi/org:id=([^:/]*)/user:id=([^:/]*)/surveys:villageCode=([^:/]*)', [noCache, handler.survey.getSurveysByVillageCode]);
  server.get('/restapi/org:id=([^:/]*)/user:id=([^:/]*)/category:locale=([^:/]*)', [noCache, handler.category.getCategoryListByLocale]);
  server.get('/restapi/org:id=([^:/]*)/user:id=([^:/]*)/category:locale=([^:/]*)/survey:id=([^:/]*)', [noCache, handler.category.getCategoryListByLocaleAndSurvey]);
  server.get('/restapi/org:id=([^:/]*)/user:id=([^:/]*)/category:id=([^:/]*)/survey:id=([^:/]*)', [noCache, handler.category.getCategoryByCategoryIdAndSurveyId]);
  server.del('/restapi/org:id=([^:/]*)/user:id=([^:/]*)/blog:id=([^:/]*)', [noCache, handler.blogs.deleteBlog]);


  server.get(/\/restapi\/org:id=([^:/]*)\/user:id=([^:/]*)\/user:type=([^:/]*)/, [noCache, handler.users.getUsersByType]);
  server.post(/\/restapi\/org:id=([^:/]*)\/user:id=([^:/]*)\/user\/profile/, [noCache, handler.users.updateUser]);
  server.get(/\/restapi\/org:id=([^:/]*)\/user:id=([^:/]*)\/category:id=([^:/]*)\/category:locale=([^:/]*)/, [noCache, handler.category.getCategoryByCategoryIdAndLocale]);
  server.put(/\/restapi\/org:id=([^:/]*)\/user:id=([^:/]*)\/blog/, [noCache, handler.blogs.saveBlog]);
  server.post(/\/restapi\/org:id=([^:/]*)\/user:id=([^:/]*)\/changesurvey:status=true\/survey:id=([^:/]*)/, [noCache, handler.survey.updateSurveyStatus]);
  server.post(/\/restapi\/org:id=([^:/]*)\/user:id=([^:/]*)\/changepassword:status=true/, [noCache, handler.users.changePassword]);
  server.put(/\/restapi\/org:id=([^:/]*)\/user:id=([^:/]*)\/usertoken/, [noCache, handler.users.saveUserToken]);





};