let logger = require('log4js').getLogger("userHandler");
let configurationService = require("../service/configurationService");
let villageService = require("../service/villageService");


exports.getDistricts = getDistricts;
exports.getVillages = getVillages;


function getVillages(req, res){
  let stateName  = req.params.state.name;
  let districtName = req.params.district.name;

  villageService.getVillageByDistrictandStateName({stateName : stateName, districtName : districtName})
  .then((villages) => res.send(200,{success : true, villages : villages}))
  .catch((error) => {
    logger.warn("Error occured while fetching villages village code",{error : error});
    return res.send(500);
  })
}


function getDistricts(req, res){
  let stateName  = req.params.state.name;

  villageService.getDistrictListByStateName({stateName : stateName})
  .then((districts) => res.send(200,{success : true, districts : districts}))
  .catch((error) => {
    logger.warn("Error occured while fetching villages village code",{error : error});
    return res.send(500);
  })
}