
let logger = require('log4js').getLogger("userHandler");
let configurationService = require("../service/configurationService");
let userService = require("../service/userService");



exports.activeUser = activeUser;
exports.login = login;
exports.register = register;
exports.forgotPassword = forgotPassword;
exports.getUsersByType = getUsersByType;
exports.saveUserToken = saveUserToken;
exports.changePassword = changePassword;
exports.suggestions = suggestions;
exports.updateUser = updateUser


function login(req, res){
  var userName = req.params.user.email;
  var password = req.body.password;

  if(! userName)
    return res.send(500);

  userService.login({userName : userName, password : password})
    .then((userData) => res.send(200,{success : true, authpayload : userData}))
    .catch((error) => {
      logger.warn("Error occured while loggin user",{error : error, userName : userName});
      return res.send(200,{success : false})
    })  
}


function register(req, res){
  let userName = req.body.userName
  let userDetails = req.body.userDetails;

  userService.register({userName : userName, userData : userDetails})
  .then((userDetails) => res.send(200,{success : true}))
  .catch((error) => {
    logger.warn("Error occured while registering user",{error : error});
    if(error.msg == "User already present"){
      return res.send(200,{success : false, msg : "UserPresent"});
    }
    res.send(500);
  })
}



function forgotPassword(req, res){
  let userName = req.params.user.email;

  userService.forgotPassword(userName)
  .then(() => res.send(200))
  .catch((error) => {
    logger.warn("Error occured while sending forgot password");
    res.send(500);
  })
}

function saveUserToken(req, res){
  let userId = req.params.user.id;
  let userToken = req.body.appToken

  userService.saveUserToken({ userId : userId, appToken : userToken})
  .then(() => res.send(200))
  .catch((error) => {
    logger.warn("Error occured while saving user data",{error :error});
    res.send(500);
  })

}

function getUsersByType(req, res){
  let userId =  req.params.user.id;
  let userType = req.params.user.type;

  userService.getAllUsersByType({userType : userType, userId : userId})
  .then((userList) => userList.map((user) => user.doc))
  .then((userListDb) => res.send(200,{success: true, users : userListDb}))
  .catch((error) => {
    logger.warn("Error occured while fethcing user list",{error : error});
    res.send(500);
  }) 
}



function suggestions(req, res){

  let userEmail = req.params.user.email;
  let suggestions = req.body.suggestions;

  userService.sendSuggestions({userEmail : userEmail, suggestions : suggestions})
  .then(() => res.send(200,{success: true}))
  .catch((error) => {
    logger.warn("Error occured while sending suggestions",{error : error});
    res.send(500);
  })

}

function changePassword(req, res){
  let userId =  req.params.user.id;
  let userPassword = req.body.userPassword;

  userService.changePassword(userId,userPassword)
  .then((userListDb) => res.send(200,{success: true}))
  .catch((error) => {
    logger.warn("Error occured while fethcing user list",error);
    res.send(500);
  }) 
}

function updateUser(req, res){

  let userId = req.params.user.id;
  let userData = req.body.userData

  userService.updateUser({userId : userId, userData : userData})
  .then(() => res.send(200,{success: true}))
  .catch((error) => {
    logger.warn("Error occured while updating user details",{error : error});
    res.send(500);
  }) 
}

function activeUser(req, res) {
	return res.send(200,{success : true, message : "Working directory"});
}
