/*
*  This module will be responsible for miscellaneous dao or db related methods /functions 
*/

var nano = require("../../nanoService")();
//var nconf = require('nconf');
var Q = require('q');

module.exports.verifyOrCreateDb = verifyOrCreateDb;

function verifyOrCreateDb(dbName, callback){
  callback = callback || function(){};
  return Q.promise(function(resolve, reject){
    // Check DB is present or not
    nano.db.get(dbName, function(err, body) {
      if(err){
        // If db file not found then create the db
        if(err.error == 'not_found' && err.statusCode === 404){
          nano.db.create(dbName, function(err, body){
            if (err)
            { 
              reject(err);
              callback(err);
            } else {
              resolve({success:true,msg:dbName + " created sucessfully"});
              callback(null,{success:true,msg:dbName + " created sucessfully"});
            }
          });
        } else {
          resolve({success:true,msg:err.reason});
          callback(null,{success:true,msg:err.reason});
        }
      } else {
        // If deb already created 
        resolve({success:true,msg:dbName + " already created"});
        callback(null,{success:true,msg:dbName + " already created"});
      }
    });
  });
}