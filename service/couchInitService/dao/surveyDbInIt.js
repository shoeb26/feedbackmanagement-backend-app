/*
*  This module will be responsible for initialization of CouchDb - the databases, 
*
*
*/
var log4js = require('log4js');
var logger = log4js.getLogger("usersDbInit");
var nconf = require('nconf');
var nano = require('../../../service/nanoService')();
var utilityService = require('../../utilityService');
var commonCouchInitUtilityFile = require('./common.js');
const Q = require('q');

// Users Document related inits
module.exports.initSurveyDoc = initSurveyDoc;

function initSurveyDoc()
{
  var defered = Q.defer();
  commonCouchInitUtilityFile.verifyOrCreateDb(nconf.get('couchDB:couchDBSurveys'), function(err, data){
    if (err)
    {
      utilityService.handleDBError(err,logger,{message: "Unable to create "+nconf.get('couchDB:couchDBSurveys')});
      return;
    }
    let db = nano.use(nconf.get('couchDB:couchDBSurveys'));
    logger.info(data.msg);
    let promises = [];
    promises.push(initFindBy(db));
    Q.all(promises).then(defered.resolve);
 });
  return defered.promise;
}


function initFindBy(db)
{
  var defered = Q.defer();
    db.get('_design/findBy', {}, function(err, body, headers){
        if (!err || err.error === "not_found")
        {
            var dbObj = new Object();
            dbObj._id = "_design/findBy";
            if (!err)
                dbObj._rev =  body._rev;
            dbObj.language = "javascript";
            dbObj.views = {
              "surveyName": {"map": "function(doc) {\n emit(doc.surveyName,doc._id);\n}"},
              "userId": {"map": "function(doc) {\n emit(doc.userId,doc._id);\n}"},
              "villageCode": {"map": "function(doc) {\n emit(doc.villageCode,doc._id);\n}"},
              "orgId": {"map": "function(doc) {\n emit(doc.orgId,doc._id);\n}"}
            };
            db.insert(dbObj, function (err, body, header) {
                if (err) 
                {
                    utilityService.handleDBError(err,logger,{message: 'Error inserting - findBy'}); 
                    defered.resolve();
                } 
                else 
                {
                    logger.info('Created '+ nconf.get('couchDB:couchDBSurveys') +
                            ' _design/findBy')
                    defered.resolve();
                }
            });
            
        }
        else
        {
            utilityService.handleDBError(err,logger,{message: 'Error - initFindBy'}); 
            defered.resolve();
        }
    });
    
  return defered.promise;
}

