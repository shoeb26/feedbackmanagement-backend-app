let userDbInit = require('./userDbInIt');
let villageDbInit = require('./villagesDbInIt');
let categoriesDbInIt = require("./categoriesDbInIt");
let surveyDbInIt = require("./surveyDbInIt");
let userCredentialsDbInIt = require("./usercredentailsDbInIt");
let surveyCategoryDbInIt = require("./surveyCategoryInIt");
let blogsDbInIt = require("./blogsDbInIt");


module.exports.initCategoryDoc = categoriesDbInIt.initCategoryDoc;
module.exports.initSurveyDoc = surveyDbInIt.initSurveyDoc;
module.exports.initVillageDoc = villageDbInit.initVillageDoc;
module.exports.initUserDoc = userDbInit.initUserDoc;
module.exports.initUserCredentialsDoc = userCredentialsDbInIt.initUserCredentialsDoc;
module.exports.initSurveyCategoryDoc = surveyCategoryDbInIt.initSurveyCategoryDoc;
module.exports.initBlogsDoc = blogsDbInIt.initBlogsDoc;



