/*
*  This module will be responsible for initialization of CouchDb - the databases, 
*
*
*/
var log4js = require('log4js');
var logger = log4js.getLogger("icUserCredentialsInit");
var nano = require("../../nanoService")();
var nconf = require('nconf');
var utilityService = require('../../utilityService');
var commonCouchInitUtilityFile = require('./common.js');
const Q = require('q');



// User Document related inits

module.exports.initUserCredentialsDoc = initUserCredentialsDoc;

function  initUserCredentialsDoc()
{
  var defered = Q.defer();
  commonCouchInitUtilityFile.verifyOrCreateDb(nconf.get('couchDB:couchDBUserCredentials'), function(err, data){
    if (err)
    {
      utilityService.handleDBError(err,logger,{message: "Unable to create "+nconf.get('couchDB:couchDBUserCredentials')});
      return;
    }
    let db = nano.use(nconf.get('couchDB:couchDBUserCredentials'));
    logger.info(data.msg);
    initDesignFindByUserName(db).then(defered.resolve);
  });
  return defered.promise;
}

function initDesignFindByUserName(db)
{
  var defered = Q.defer();
    db.get('_design/findByUserName', {}, function(err, body, header){
        if (!err || err.error === "not_found")
        {
            var dbObj = new Object();
            dbObj._id = "_design/findByUserName";
            if (!err)
                dbObj._rev =  body._rev;
            dbObj.language = "javascript";
            dbObj.views = {"userName": {"map": "function(doc){if(doc.userName) {emit(doc.userName.toLowerCase(), doc );}}"}};
            db.insert(dbObj, function (err, body, header) {
                if (err) 
                {
                    utilityService.handleDBError(err,logger,{message: 'Error inserting - initDesignFindByEmail'});
                    defered.resolve();
                } 
                else 
                {
                    logger.info('Created '+ nconf.get('couchDB:couchDBUserCredentials') +
                            ' _design/findByUserName')
                    defered.resolve();        
                }
              });
        }
        else
        {
            utilityService.handleDBError(err,logger,{message: 'Error - couchDBUserCredentials'});
            defered.resolve();
        }
    });
  return defered.promise;
    
}