/*
*  This module will be responsible for initialization of CouchDb - the databases, 
*
*
*/
var log4js = require('log4js');
var logger = log4js.getLogger("usersDbInit");
var nconf = require('nconf');
var nano = require('../../../service/nanoService')();
var utilityService = require('../../utilityService');
var commonCouchInitUtilityFile = require('./common.js');
const Q = require('q');

// Users Document related inits
module.exports.initUserDoc = initUserDoc;

function initUserDoc()
{
  var defered = Q.defer();
  commonCouchInitUtilityFile.verifyOrCreateDb(nconf.get('couchDB:couchDBUsers'), function(err, data){
    if (err)
    {
      utilityService.handleDBError(err,logger,{message: "Unable to create "+nconf.get('couchDB:couchDBUsers')});
      return;
    }
    let db = nano.use(nconf.get('couchDB:couchDBUsers'));
    logger.info(data.msg);
    let promises = [];
    promises.push(initFindBy(db));
    Q.all(promises).then(defered.resolve);
 });
  return defered.promise;
}


function initFindBy(db)
{
  var defered = Q.defer();
    db.get('_design/findBy', {}, function(err, body, headers){
        if (!err || err.error === "not_found")
        {
            var dbObj = new Object();
            dbObj._id = "_design/findBy";
            if (!err)
                dbObj._rev =  body._rev;
            dbObj.language = "javascript";
            dbObj.views = {
              "userName": {"map": "function(doc) {\n emit(doc.userName,doc._id);\n}"},
              "userNameAndUserType": {"map": "function(doc) {\n emit([doc.userName,doc.userType],doc._id);\n}"},
              "userType": {"map": "function(doc) {\n emit(doc.userType,doc._id);\n}"}
            };
            db.insert(dbObj, function (err, body, header) {
                if (err) 
                {
                    utilityService.handleDBError(err,logger,{message: 'Error inserting - findBy'}); 
                    defered.resolve();
                } 
                else 
                {
                    logger.info('Created '+ nconf.get('couchDB:couchDBUsers') +
                            ' _design/findBy')
                    defered.resolve();
                }
            });
            
        }
        else
        {
            utilityService.handleDBError(err,logger,{message: 'Error - initFindBy'}); 
            defered.resolve();
        }
    });
    
  return defered.promise;
}

