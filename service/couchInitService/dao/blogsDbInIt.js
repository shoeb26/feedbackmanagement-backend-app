/*
*  This module will be responsible for initialization of CouchDb - the databases, 
*
*
*/
var log4js = require('log4js');
var logger = log4js.getLogger("blogDbInit");
var nconf = require('nconf');
var nano = require('../../../service/nanoService')();
var utilityService = require('../../utilityService');
var commonCouchInitUtilityFile = require('./common.js');
const Q = require('q');

// Users Document related inits
module.exports.initBlogsDoc = initBlogsDoc;

function initBlogsDoc()
{
  var defered = Q.defer();
  commonCouchInitUtilityFile.verifyOrCreateDb(nconf.get('couchDB:couchDBBlogs'), function(err, data){
    if (err)
    {
      utilityService.handleDBError(err,logger,{message: "Unable to create "+nconf.get('couchDB:couchDBBlogs')});
      return;
    }
    let db = nano.use(nconf.get('couchDB:couchDBBlogs'));
    logger.info(data.msg);
    let promises = [];
    promises.push(initFindBy(db));
    Q.all(promises).then(defered.resolve);
 });
  return defered.promise;
}


function initFindBy(db)
{
  var defered = Q.defer();
    db.get('_design/findBy', {}, function(err, body, headers){
        if (!err || err.error === "not_found")
        {
            var dbObj = new Object();
            dbObj._id = "_design/findBy";
            if (!err)
                dbObj._rev =  body._rev;
            dbObj.language = "javascript";
            dbObj.views = {
              "userId": {"map": "function(doc) {\n emit(doc.userId,doc._id);\n}"}
            };
            db.insert(dbObj, function (err, body, header) {
                if (err) 
                {
                    utilityService.handleDBError(err,logger,{message: 'Error inserting - findBy'}); 
                    defered.resolve();
                } 
                else 
                {
                    logger.info('Created '+ nconf.get('couchDB:couchDBBlogs') +
                            ' _design/findBy')
                    defered.resolve();
                }
            });
            
        }
        else
        {
            utilityService.handleDBError(err,logger,{message: 'Error - initFindBy'}); 
            defered.resolve();
        }
    });
    
  return defered.promise;
}

