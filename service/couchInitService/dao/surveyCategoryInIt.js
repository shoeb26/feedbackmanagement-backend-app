/*
*  This module will be responsible for initialization of CouchDb - the databases, 
*
*
*/
var log4js = require('log4js');
var logger = log4js.getLogger("usersDbInit");
var nconf = require('nconf');
var nano = require('../../../service/nanoService')();
var utilityService = require('../../utilityService');
var commonCouchInitUtilityFile = require('./common.js');
const Q = require('q');

// Users Document related inits
module.exports.initSurveyCategoryDoc = initSurveyCategoryDoc;

function initSurveyCategoryDoc()
{
  var defered = Q.defer();
  commonCouchInitUtilityFile.verifyOrCreateDb(nconf.get('couchDB:couchDBSurveyCategory'), function(err, data){
    if (err)
    {
      utilityService.handleDBError(err,logger,{message: "Unable to create "+nconf.get('couchDB:couchDBSurveyCategory')});
      return;
    }
    let db = nano.use(nconf.get('couchDB:couchDBSurveyCategory'));
    logger.info(data.msg);
    let promises = [];
    promises.push(initFindBy(db));
    Q.all(promises).then(defered.resolve);
 });
  return defered.promise;
}


function initFindBy(db)
{
  var defered = Q.defer();
    db.get('_design/findBy', {}, function(err, body, headers){
        if (!err || err.error === "not_found")
        {
            var dbObj = new Object();
            dbObj._id = "_design/findBy";
            if (!err)
                dbObj._rev =  body._rev;
            dbObj.language = "javascript";
            dbObj.views = {
              "categoryName": {"map": "function(doc) {\n emit(doc.categoryName,doc._id);\n}"},
              "surveyIdAndCategoryId": {"map": "function(doc) {\n emit([doc.surveyId, doc.categoryId],doc._id);\n}"},
              "surveyId": {"map": "function(doc) {\n emit(doc.surveyId,doc._id);\n}"}
            };
            db.insert(dbObj, function (err, body, header) {
                if (err) 
                {
                    utilityService.handleDBError(err,logger,{message: 'Error inserting - findBy'}); 
                    defered.resolve();
                } 
                else 
                {
                    logger.info('Created '+ nconf.get('couchDB:couchDBSurveyCategory') +
                            ' _design/findBy')
                    defered.resolve();
                }
            });
            
        }
        else
        {
            utilityService.handleDBError(err,logger,{message: 'Error - initFindBy'}); 
            defered.resolve();
        }
    });
    
  return defered.promise;
}

