const Q = require('q');
var couchInitDao = require("./dao");

function initCouch()
{
  var defered = Q.defer();
  let promises = [];
    // This will call the couchDb methods to init the 
    // various databases, the views, mappings , etc.
    
    // First init the master Data
    
    promises.push(couchInitDao.initVillageDoc());
    promises.push(couchInitDao.initUserDoc());
    promises.push(couchInitDao.initSurveyDoc());
    promises.push(couchInitDao.initCategoryDoc());
    promises.push(couchInitDao.initUserCredentialsDoc());
    promises.push(couchInitDao.initSurveyCategoryDoc());
    promises.push(couchInitDao.initBlogsDoc());
    
    
    Q.all(promises).then(function(results){
      defered.resolve(results); 
    });
    return defered.promise;
}

module.exports.init = initCouch;