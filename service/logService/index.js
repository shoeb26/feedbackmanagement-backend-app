var log4js = require('log4js');
var configurationService = require('../../service/configurationService');


module.exports = log4js;

(function configureLog4js() {

  var log4jsConfiguration = configurationService.get('log4js');
  //Switch to an array, the file has a map for easy overriding.
  log4jsConfiguration.appenders = log4jsConfiguration.appenders && Object.values(log4jsConfiguration.appenders);
  // configure log4js once at startup
  log4js.configure(log4jsConfiguration);

})();