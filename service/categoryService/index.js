const moment  = require("moment");
const categoryDao = require("./dao");
const surveyService = require('../surveyService');
const logger = require('log4js').getLogger("categoryService");
const Q = require('q');

exports.saveCategory = categoryDao.saveCategory;
exports.getCategoryByCategoryId = categoryDao.getCategoryByCategoryId;
exports.getCategoryListByLocale = getCategoryListByLocale;
exports.getCategoryByCategoryIdAndLocale = getCategoryByCategoryIdAndLocale;
exports.getCategoryByCategoryIdAndSurveyId = getCategoryByCategoryIdAndSurveyId;
exports.getCategoryListByLocaleAndSurvey = getCategoryListByLocaleAndSurvey;



function getCategoryListByLocale(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.locale)
      throw new Error("Locale not defined");

    categoryDao.getCategoryListByLocale({locale : opts.locale})
      .then((categories) => categories.map((category) => {return {categoryId : category.doc._id, categoryName : category.doc.categoryName}}))
      .then(resolve)
      .catch((error) => {
        logger.warn("getCategoryListByLocale() - Error occured while fetching categories",{error : error,locale :locale});
        return reject(error)
      });
  })
}


function getCategoryListByLocaleAndSurvey(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.locale)
      throw new Error("Locale not defined");

    if(! opts.surveyId)
      throw new Error("Survey id not defined");

      let categoriesDb;
      
      categoryDao.getCategoryListByLocale({locale : opts.locale})
      .then((category) =>  {
        categoriesDb = category;
        return;
      })
      .then(() => {
        return surveyService.getSurveyCategoryBySurveyId({surveyId : opts.surveyId})
      })
      .catch((error) => {
        if(error.msg == "Record not present"){
          return Q.resolve();
        }

        return reject(error);
      })
      .then((surveyCategoryDetails) => {
        if(!! surveyCategoryDetails){
          let categoryDb = compareCategoriesAndSurveyCategories(categoriesDb,surveyCategoryDetails);
          return resolve(categoryDb);
        } else{
          return resolve(categoriesDb.map((category) => {return {categoryId : category.doc._id, categoryName : category.doc.categoryName,bg : "yellow"}}));
        }
      })
      .catch((error) => {
        logger.warn("getCategoryListByLocaleAndSurvey() - Error occured while fetching categories",{error : error,locale :locale});
        return reject(error)
      });
  })
}



function compareCategoriesAndSurveyCategories(categories, surveyCategorys){
  try{
    return categories.map((categoryDoc) => {
      let surveyCategory = surveyCategorys.filter((surveyCategory) => surveyCategory.doc.categoryId == categoryDoc.doc._id);
      if(surveyCategory.length == 0){
        return {categoryId : categoryDoc.doc._id, categoryName : categoryDoc.doc.categoryName,bg : "yellow"};
      } else if(surveyCategory.length > 0){
        if(surveyCategory[0].doc.subCategoryDetails.length == categoryDoc.doc.subCategoryDetails.length) {
          return {categoryId : categoryDoc.doc._id, categoryName : categoryDoc.doc.categoryName,bg : "green"};
        } else {
          return {categoryId : categoryDoc.doc._id, categoryName : categoryDoc.doc.categoryName,bg : "red"};
        } 
      } 
    });
  }catch(e){
    logger.warn("Error occured while compareCategoriesAndSurveyCategories",{error : e});
    return categories.map((category) => {return {categoryId : category.doc._id, categoryName : category.doc.categoryName,bg : "yellow"}})
  }
}

function getCategoryByCategoryIdAndLocale(opts){
  return Q.promise((resolve, reject) => { 
    if(! opts.categoryId)
      throw new Error("Category id not defined")

    if(! opts.locale)
      throw new Error("Locale not defined");

    categoryDao.getCategoryByCategoryId({categoryId : opts.categoryId})
      .then((category) => resolve(category))
      .catch((error) => {
        logger.warn("getCategoryByCategoryIdAndLocale() - Error occured while fetching doc by category Id and locale ",
          {categoryId : opts.categoryId, locale : opts.locale})
        return reject(error);
      })
  });
}

function getCategoryByCategoryIdAndSurveyId(opts){
  return Q.promise((resolve, reject) => {
    if(! opts.surveyId)
      throw new Error("Survey id not defined");

    if(! opts.categoryId)
      throw new Error("Category id not defined");

    
    let categoryDetails ;

    categoryDao.getCategoryByCategoryId({categoryId : opts.categoryId})
      .then((category) => {
        categoryDetails = category;
        return surveyService.getSurveyBySurveyIdAndCategoryId({surveyId : opts.surveyId,  categoryId : opts.categoryId});
      })
      .catch((error) => {
        if(error.msg == "Record not present"){
          return false;
        }

        return reject(error);
      })
      .then((surveyCategoryData) => {
        return mergeCategoryDetailsAndSurveyDetails(surveyCategoryData, categoryDetails);
      })
      .then(resolve)
      .catch((error) => {
        logger.warn("Error occured while fetching category details",{error : error});
        return reject(error);
      })
  })
}


function mergeCategoryDetailsAndSurveyDetails(surveyDetails,categoryDetails){
  let mergedCategoryArray, startAtIndex =0;
  if(! surveyDetails){
    mergedCategoryArray = categoryDetails.subCategoryDetails.reduce((arr,category) => {

      let categoryName = category.categoryName;
      (category.details || []).forEach((detail) => {
        let question = {};
        question.name = detail.name;
        question.categoryName = categoryName;
        question.pointsGiven = 0;
        question.centre = "";
        question.points = detail.points;
        question.comments = detail.comments;
        arr.push(question);
      })
      return arr;
    },[]);
  } else {
     mergedCategoryArray = categoryDetails.subCategoryDetails.reduce((arr,category) => {

      let categoryName = category.categoryName;
      let surveySubCategoryDetails =  surveyDetails.subCategoryDetails.filter(
        (subCategory) => subCategory.categoryName == categoryName);


     

      if(surveySubCategoryDetails.length > 1){
        surveySubCategoryDetails.splice(1,surveySubCategoryDetails.length) 
      }

      (category.details || []).forEach((detail, index) => {
        let surveyDetail = [];
        let question = {};

        if(surveySubCategoryDetails.length > 0){
          surveyDetail = surveySubCategoryDetails[0].details.filter((subDetail) => subDetail.name == detail.name);
          if(surveyDetail.length > 0){
            startAtIndex = index + 1;
          }
        }
        
        question.name = detail.name;
        question.categoryName = categoryName;
        question.pointsGiven = (surveyDetail[0] || {}).pointsGiven || 0;
        question.centre = (surveyDetail[0] || {}).centre || "";
        question.points = detail.points;
        question.comments = detail.comments;
        arr.push(question);
      })
      return arr;
    },[]);
  }

  return {mergedCategoryArray : mergedCategoryArray, startAtIndex : startAtIndex}

}