const log4js = require('log4js');
const logger = log4js.getLogger("categoryDao");
const configurationService = require('../../configurationService');
const db = require('../../nanoService')(configurationService.get('couchDB:couchDBCategories'));
const utilityService = require('../../utilityService');
const moment = require('moment');
const Q = require('q');


exports.saveCategory = saveCategory;
exports.getCategoryByCategoryId = getCategoryByCategoryId;
exports.getCategoryListByLocale = getCategoryListByLocale;


function saveCategory(opts){
  return Q.promise((resolve, reject) => {
    if(! opts.locale)
      throw new Error("Locale not defined");

    if(! opts.categoryDoc)
      throw new Error("CategoryDoc not defined");

    let categoryDoc = opts.categoryDoc;
    categoryDoc.createdAt = moment().toISOString();
    categoryDoc.locale = opts.locale;

     db.insert(categoryDoc, function(err, body, header){
      if (err) {
        utilityService.handleDBError(err,logger,{ message:"Error inserting survey "});
        return reject(err);
      }
      return resolve(body);
    });
  })
}

function getCategoryByCategoryId(opts){
  return Q.promise((resolve, rejcet) => {

    if(! opts.categoryId)
      throw new Error("CategoryId not defined");

    db.get(opts.categoryId,(error, result) => {
      if(error){
        logger.warn("getCategoryByCategoryId - Error occured while fetching category details",{categoryId : opts.categoryId});
        return reject(error);
      }
      console.log("result", result);
      return resolve(result);
    })
  })
}


function getCategoryListByLocale(opts){
  return Q.promise((resolve, reject) => {
    if(! opts.locale)
      throw new Error("Locale not defined");


    db.view("findBy", "locale",{key : opts.locale,  include_docs: true}, (err , data) => {
      if (err) {
          utilityService.handleDBError(err,logger,{message: 'could not fetch category details',locale: opts.locale});
          return reject(err);
      } else {
        if (data.rows.length > 0 && typeof data.rows[0].doc !== 'undefined')  {   
          logger.trace("fetched category details by locale from db ",{locale: opts.locale});
          return resolve(data.rows);
        } else {
          logger.warn('catgeory not found for locale',{locale :opts.locale});
          return reject({success : false, msg :  "data not present"});
        }
      }
    })
  })
}