/*
 * This module will be the service module responsible for parameterizing the
 * rest API calls such that all the path parameters are available in req.params.<pathSubject>.<predicates>
 * For Example: 
 * API Call:
 * /restapi/org:id=123:name=abc:section=567:a=b/user:id=345:name=naveen:role=admin
 * 
 * Will be available as :
 *  req.param.org.id : 123
 *  req.param.org.name: 'abc'
 *  req.param.org.section: 567
 *  req.param.org.a: b
 *  req.param.user.id:  345
 *  req.param.user.name: 'naveen'
 *  req.param.user.role: 'admin'
 */


var  XRegExp = require('xregexp').XRegExp;
var log4js = require('log4js');
var nconf = require('nconf');
var logger = log4js.getLogger("parameterizerService");
    
//logger.setLevel(nconf.get('log4js:levels:parameterizerService'));

function parameterize(req, res, next)
{  
    // let us first split the url into path segments
    var path = XRegExp.split(req.url,"\?")[0];
    
    if (logger.isDebugEnabled())
        logger.debug("Request Path before processing:"+path);
    
    if (logger.isDebugEnabled())
        logger.debug("Request Url before processing:"+req.url);
    
    var pathSegments = XRegExp.split(path,"\/");
    
    if (logger.isDebugEnabled())
        logger.debug("req.params before processing:" + JSON.stringify(req.params));
    
    var pathSegment;
    
    for (pathSegment in pathSegments)
    {
        if (pathSegments[pathSegment].length == 0)
            continue;
        addPathParams(req, pathSegments[pathSegment]);
    }
    if (logger.isDebugEnabled())
        logger.debug("req.params after processing:" + JSON.stringify(req.params));
    
    if (logger.isDebugEnabled())
        logger.debug("Request Url after processing:"+req.url);
    
    next();
}

function addPathParams(req, pathSegment)
{
    
    
    // let us first check if this pathSegment needs to be parameterized
    // if the path segment does not contain ":" then it does not need to be paraeterized
    if (logger.isInfoEnabled())
        logger.debug("path Segment to process:" + pathSegment);
    
    if (pathSegment.indexOf(":") == -1)
        return;
    
    // let us parameterize the segment
    var pathSegmentRegex = XRegExp.build("^({{pathSubject}})(:?({{pathPredicate}}))*",
            { pathSubject: /[^:]+/,
              pathPredicate: /[^:]+/
            }, "x");

    var pathSubject = XRegExp.exec(pathSegment, pathSegmentRegex).pathSubject;
    if (logger.isDebugEnabled())
        logger.debug("path Segment to process:" + pathSubject);
    
    var predicateKeyValRegex = XRegExp("(?<key>[^=]+)=(?<value>.*)");
    
    var predicate;
    var pathPredicates = new Array();

    while((predicate = XRegExp.exec(pathSegment,pathSegmentRegex).pathPredicate) != undefined)
    {
        if (logger.isDebugEnabled())
            logger.debug("Predicate :"+predicate);
        
        pathSegment = pathSegment.replace(predicate,"");
        if (logger.isDebugEnabled())
            logger.debug("Path Segment after Predicate Replacement:"+pathSegment);
        
        var keyVals = XRegExp.exec(predicate,predicateKeyValRegex);
        if (logger.isDebugEnabled())
            logger.debug("KeyVals identified:" + JSON.stringify(keyVals));
        
        var pathPredicate = new Object();
        if(keyVals!= null){
          pathPredicate.key = keyVals.key;
          pathPredicate.value = keyVals.value;
          pathPredicates.push(pathPredicate);
        }else{
          pathPredicate.key = predicate;
          pathPredicates.push(pathPredicate);
        }
    }
    
    if (logger.isInfoEnabled())
        logger.debug('Adding predicates: '+ JSON.stringify(pathPredicates));
    
    if (pathPredicates.length == 0)
        return;
    
    // Now that we have the predicates as key-value pairs 
    // let us add them to the request
    // We will check if the pathSubject already exists ... 
    if (req.params[pathSubject] == undefined)
        req.params[pathSubject] = new Object();
    
    var predicateKeyVal;
    for (predicateKeyVal in pathPredicates)
    {
        var kVPair = pathPredicates[predicateKeyVal];
        req.params[pathSubject][kVPair.key] = unescape(kVPair.value);
    }
}

module.exports.parameterize = parameterize;
