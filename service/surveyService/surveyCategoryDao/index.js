const log4js = require('log4js');
const logger = log4js.getLogger("surveyCatgeoryDao");
const configurationService = require('../../configurationService');
const db = require('../../nanoService')(configurationService.get('couchDB:couchDBSurveyCategory'));
const utilityService = require('../../utilityService');
const moment = require('moment');
const Q = require('q');



exports.getSurveyBySurveyIdAndCategoryId = getSurveyBySurveyIdAndCategoryId;
exports.addCategoryDetails = addCategoryDetails;
exports.getSurveyCategoryBySurveyId = getSurveyCategoryBySurveyId;


function getSurveyBySurveyIdAndCategoryId(opts){
  return Q.promise((resolve, reject) => {
    if(! opts.surveyId)
      throw new Error("Survey id missing");

    if(! opts.categoryId)
      throw new Error("Category id missing");

    let key = [opts.surveyId, opts.categoryId];

    console.log(key);

    db.view('findBy','surveyIdAndCategoryId', {key : key, include_docs: true}, (error, result) => {
      if(error){
        logger.warn("Error occured while fetching details",{error : error, surveyId : opts.surveyId, categoryId :opts.categoryId});
        return reject(error)
      }


      if (result.rows.length > 0 && typeof result.rows[0].doc !== 'undefined')  {   
        return resolve(result.rows[0].doc);
      } else {
        logger.warn('survey not found for',{surveyId : opts.surveyId, categoryId :opts.categoryId});
        return reject({success : false, msg :  "Record not present"});
      }
    })
  })
}


function addCategoryDetails(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.categoryDetails)
      throw new Error("Category details not present")

    if(opts.categoryDetails._id){
      opts.categoryDetails.updateddAt = moment().toISOString();
    } else {
      opts.categoryDetails.createdAt = moment().toISOString();
    }

    db.insert(opts.categoryDetails, function(err, body, header){
      if (err) {
        utilityService.handleDBError(err,logger,{ message:"Error inserting user "});
        return reject(err);
      }
      
      return resolve(body);
    });
  })
}


function getSurveyCategoryBySurveyId(opts){
  return Q.promise((resolve, reject) => {
    
    if(! opts.surveyId)
      throw new Error("Survey id missing");

    let key = opts.surveyId;

    db.view('findBy','surveyId', {key : key, include_docs: true}, (error, result) => {
      if(error){
        logger.warn("Error occured while fetching details",{error : error, surveyId : opts.surveyId, categoryId :opts.categoryId});
        return reject(error)
      }

      if (result.rows.length > 0 && typeof result.rows !== 'undefined')  {   
        return resolve(result.rows);
      } else {
        logger.warn('user not found for userName',{surveyId :opts.surveyId, err:error});
        return reject({success : false, msg :  "Record not present"});
      }
    })
  })

}