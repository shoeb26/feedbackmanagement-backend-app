const log4js = require('log4js');
const logger = log4js.getLogger("surveyDao");
const configurationService = require('../../configurationService');
const db = require('../../nanoService')(configurationService.get('couchDB:couchDBSurveys'));
const utilityService = require('../../utilityService');
const moment = require('moment');
const Q = require('q');

exports.addSurvey = addSurvey;
exports.getSurveyBySurveyId = getSurveyBySurveyId;
exports.getSurveyByUserId = getSurveyByUserId;
exports.getSurveyByOrgId = getSurveyByOrgId;
exports.getSurveyByVillageCode = getSurveyByVillageCode;






function addSurvey(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.surveyData)
      throw new Error('Survey Details missing');

    if(opts.surveyData._id){
      opts.surveyData.updatedAt = moment().toISOString();
    } else {
      opts.surveyData.createdAt = moment().toISOString();
    }

    db.insert(opts.surveyData, function(err, body, header){
      if (err) {
        utilityService.handleDBError(err,logger,{ message:"Error inserting survey "});
        return reject(err);
      }
      return resolve(body);
    });
  })
}


function getSurveyBySurveyId(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.surveyId)
      throw new Error("Survey id not present")

    db.get(opts.surveyId,(error, result) => {
      if(error){
        logger.warn("getSurveyBySurveyId - Error occured while fetching survey details");
        return reject(error);
      }
      
      return resolve(result);
    })
  })
}

function getSurveyByUserId(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.userId)
      throw new Error("User id not present");

    db.view('findBy','userId',{key : opts.userId, include_docs: true},(error , data) => {
      if (error) {
          utilityService.handleDBError(error,logger,{message: 'could not fetched user details',userId: opts.userId});
          return reject(error);
      } else {

        if (data.rows.length > 0)  {   
            logger.trace("fetched user details by userId from db ",{userId: opts.userId, user :data.rows[0].doc});
            return resolve(data.rows);
        } else {
            logger.warn('user not found for userId',{userId : opts.userId, error:error});
            return reject({success : false, msg :  "User not present"});
        }
      }
    })
  })
}

function getSurveyByOrgId(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.orgId)
      throw new Error("Og Id not present");

    db.view('findBy','orgId',{key : opts.orgId, include_docs: true},(error , data) => {
      if (error) {
          utilityService.handleDBError(error,logger,{message: 'could not fetched user details',orgId: opts.orgId});
          return reject(error);
      } else {

        if (data.rows.length > 0 )  {   
            logger.trace("fetched survey details by orgId from db ",{orgId: opts.orgId});
            return resolve(data.rows);
        } else {
            logger.warn('survey not found for orgId',{orgId :opts.orgId});
            return reject({success : false, msg :  "User not present"});
        }
      }
    })
  })
}

function getSurveyByVillageCode(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.villageCode)
      throw new Error("villageCode Is not present");

    db.view('findBy','villageCode',{key : opts.villageCode, include_docs: true},(error , data) => {
      if (error) {
          utilityService.handleDBError(error,logger,{message: 'could not fetched user details',orgId: opts.orgId});
          return reject(error);
      } else {

        if (data.rows.length > 0 )  {   
            logger.trace("fetched survey details by orgId from db ",{orgId: opts.orgId});
            return resolve(data.rows);
        } else {
            logger.warn('survey not found for orgId',{orgId :opts.orgId});
            return reject({success : false, msg :  "User not present"});
        }
      }
    })
  })
}