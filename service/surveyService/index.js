const moment  = require("moment");
const logger = require('log4js').getLogger("surveyService");
const surveyDao = require('./dao');
const surveyCategoryDao = require('./surveyCategoryDao');
const configurationService  = require('../configurationService');
const userService = require('../userService');
const Q = require('q');
const adminOrgId =  configurationService.get('adminOrgId');
const surveyLimit = configurationService.get('surveyLimit');


exports.createSurvey = createSurvey;
exports.updateSurvey = updateSurvey;
exports.getSurveyBySurveyIdAndCategoryId = getSurveyBySurveyIdAndCategoryId;
exports.getSurveyBySurveyId = getSurveyBySurveyId;
exports.getSurveyListByUserId = getSurveyListByUserId;
exports.updateSurveyStatus = updateSurveyStatus;
exports.getChartData = getChartData;
exports.getSurveysByVillageCode = getSurveysByVillageCode;
exports.getSurveyCategoryBySurveyId = surveyCategoryDao.getSurveyCategoryBySurveyId;


function createSurvey(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.userId)
      throw new Error("USer id not defined");

    if(! opts.surveyDetails.surveyName)
      throw new Error("Surery name not defined");

    if(! opts.userDetails)
      throw new Error("User details not defined");


    let surveyDetailsDb = {
      surveyName : opts.surveyDetails.surveyName,
      userId : opts.userId,
      surveyedPersonName : opts.userDetails.userName,
      villageName : opts.surveyDetails.villageName,
      villageId : opts.surveyDetails.villageId,
      status : "draft",
      orgId : adminOrgId,
      surveyedPersonDetails : {
        phoneNo : opts.userDetails.phoneNo || "NA",
        address : opts.userDetails.address || "NA"
      }    
    }

    checkUserSurveyCount({userId : opts.userId})
    .then((userStatus) => {
      if(userStatus)
        return surveyDao.addSurvey({surveyData : surveyDetailsDb})

      return reject({success : false, msg :  "Survey limit reached"})
    })
    .then((surveyDetails) => resolve(surveyDetails))
    .catch((error) => {
      logger.warn("createSurvey - Error occurred while creating a survey",{error : error, userId : opts.userDetails.userId});
      return reject(error);
    })
  })
}



function checkUserSurveyCount(opts){
  return Q.promise((resolve, reject) => {
    userService.getUserByUserId({userId : opts.userId})
    .then((user) => (user.userType == "registeredUser") ? surveyDao.getSurveyByUserId({userId : opts.userId}) : [])
    .catch((error) => {
      if (error.msg=="User not present") {
        logger.warn('getSurveyListByUserId - user not found for userId',{userId : opts.userId, error:error});
        return resolve(true);
      }
    })
    .then(userSurveys => (userSurveys.length < surveyLimit) ? true : false)
    .then(resolve)
    .catch(reject) 
  })
}

function updateSurvey(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.surveyId)
      throw new Error("survey id not defined");

    if(! opts.surveyDetails)
      throw new Error("survey details not defined");

    if(! opts.surveyDetails.categoryName)
      throw new Error("survey details not defined");

    let subCategoryDetails = opts.surveyDetails.subCategoryDetails;
    let categoryName = opts.surveyDetails.categoryName;
    let surveyId = opts.surveyId;
    let categoryId = opts.categoryId;

    let unMatchedCategoryObject = {};
    surveyCategoryDao.getSurveyBySurveyIdAndCategoryId({surveyId : opts.surveyId,  categoryId : opts.categoryId})
      .then((surveyCategoryDetails) => {
         surveyCategoryDetails.subCategoryDetails
          .filter((subCategory) => {
            if(subCategory.categoryName == subCategoryDetails.categoryName){
              return true;
            } else {
              unMatchedCategoryObject.categoryName = subCategoryDetails.categoryName;
              unMatchedCategoryObject.details = [];
              unMatchedCategoryObject.details = unMatchedCategoryObject.details.concat(subCategoryDetails.details);
            }
          })
          .map((subCategory) => {
              if(! Object.keys(subCategory.details).length){
                subCategory.details = {
                  subCategoryDetails : [].concat(subCategoryDetails.details)
                }
              } else {

                subCategory.details.forEach((detail) => {
                  let arrayIndex;
                  let subDetails = subCategoryDetails.details.filter((uiDetail, index) => {
                    if(uiDetail.name == detail.name){
                      arrayIndex = index;
                      return true;
                    }
                    return false;
                  });

                  if(typeof arrayIndex!= "undefined"){
                    subCategoryDetails.details.splice(arrayIndex,1);
                  }
                  if(subDetails.length > 0){
                    detail.pointsGiven = subDetails[0].pointsGiven;
                    detail.centre = subDetails[0].centre;
                  }
                })
                subCategory.details = subCategory.details.concat(subCategoryDetails.details);
              }

          })

          if(Object.keys(unMatchedCategoryObject).length > 0){
            surveyCategoryDetails.subCategoryDetails.push(unMatchedCategoryObject);
          }
          return surveyCategoryDetails;
      })
      .catch((error) => {
        if(error.msg == "Record not present"){
          return {
            categoryName : categoryName,
            surveyId : surveyId,
            categoryId : categoryId,
            orgId : adminOrgId,
            subCategoryDetails : [{
              categoryName : subCategoryDetails.categoryName,
              details : subCategoryDetails.details || []
            }]
          }
        }

        return reject(error);
      })
      .then((categoryData) => {
        return surveyCategoryDao.addCategoryDetails({categoryDetails : categoryData})
      })
      .then(resolve)
      .catch((error) => {
        logger.warn("Error occurred saving data for survey subcategories",{error : error});
        return reject(error);
      })
  })
}


function getSurveyBySurveyIdAndCategoryId(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.surveyId)
      throw new Error("Survey id not present");

    if (! opts.categoryId) 
      throw new Error("Category id not present");

    surveyCategoryDao.getSurveyBySurveyIdAndCategoryId({surveyId : opts.surveyId, categoryId : opts.categoryId})
      .then(resolve)
      .catch(function(error){
        logger.warn("getSurveyBySurveyIdAndCategoryId -  Error occurred while fetching category details",{surveyId : opts.surveyId,
         categoryId : opts.categoryId});
        return reject(error);
      })
    
  })
}

function getSurveyBySurveyId(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.surveyId)
      throw new Error("survey id not defined");

    let surveyDetails = {};
    surveyDao.getSurveyBySurveyId({surveyId :opts.surveyId})
      .then((survey) => {
        surveyDetails = survey;
        return surveyCategoryDao.getSurveyCategoryBySurveyId({surveyId : opts.surveyId})
      })
      .catch((error) => {
        if(error.msg == "Record not present"){
          return Q.resolve();
        }

        return reject(error);
      })
      .then((surveyCategoryDetails) => {

        if(!! surveyCategoryDetails){
          surveyDetails.categoryDetails = [];
          surveyCategoryDetails.forEach((doc) => {
            surveyDetails.categoryDetails.push(doc);
          });
        }

        return resolve(surveyDetails);
      })
      .catch((error) => {
        logger.warn("getSurveyBySurveyId() - Error occurred while fetching survey details",{error : error});
        return reject(error);
      });
  })
}


function getSurveyListByUserId(opts){
  return Q.promise((resolve , reject) => {

    if(! opts.userId)
      throw new Error("User id not defined");


    userService.getUserByUserId({userId : opts.userId})
      .then((user) => {
        if(user.userType == "Admin"){
          return surveyDao.getSurveyByOrgId({orgId : adminOrgId});
        } else {
          return surveyDao.getSurveyByUserId({userId : opts.userId});
        }
      })
      .then((surveys) => surveys.map((survey) => survey.doc))
      .then(resolve)
      .catch((error) => {
        if (error.msg=="User not present") {
          logger.warn('getSurveyListByUserId - user not found for userId',{userId : opts.userId, error:error});
          return reject({success : false, msg :  "User not present"});
        }
        logger.warn("Error occurred while fetching survey details",{error : error});
        return reject(error);
      })
  })
}

function getSurveysByVillageCode(opts){
  return Q.promise((resolve , reject) => {

    if(! opts.villageCode)
      throw new Error("villageCode not defined");

    if(! opts.userId)
      throw new Error("User id not defined");

    userService.getUserByUserId({userId : opts.userId})
      .then((user) => {
          return surveyDao.getSurveyByVillageCode({villageCode : opts.villageCode});
      })
      .then((surveys) => surveys.map((survey) => survey.doc))
      .then(resolve)
      .catch((error) => {
        if (error.msg=="User not present") {
          logger.warn('getSurveyListByUserId - user not found for userId',{userId : opts.userId, error:error});
          return reject({success : false, msg :  "User not present"});
        }
        logger.warn("Error occurred while fetching survey details",{error : error});
        return reject(error);
      })
  })
}

function updateSurveyStatus(opts){
  return Q.promise((resolve , reject) => {

    if(! opts.surveyId)
      throw new Error("Survey id not defined");

    surveyDao.getSurveyBySurveyId({surveyId :opts.surveyId})
      .then((survey) => {
        survey.status = "completed";
        return surveyDao.addSurvey({surveyData : survey})
      })
      .then(resolve)
      .catch(reject);
  })
}


function getChartData(opts){

  if(opts.villageCode)
    throw new Error("Village id not defined")


  if(opts.chartType)
    throw new Error("Chart type not defined")


  if(opts.userId)
    throw new Error("User id not defined");


  return getSurveyDetailsData(opts);   
      
}


function getSurveyDetailsData(opts){
  return Q.promise((resolve, reject) => {
    surveyDao.getSurveyByOrgId({orgId : adminOrgId})
    .then((surveyData) => surveyData.filter((survey) => survey.villageCode == opts.villageCode && survey.status == "completed"))
    .then((filteredSurveys) => {
      let filteredSurveysResults =  filteredSurveys.reduce((acc , survey) => {
        return surveyCategoryDao.getSurveyCategoryBySurveyId({surveyId : survey._id})
        .then((surveyData) => survey.categories = surveyData).catch((error) => []);
      },[]);

      return Q.all(filteredSurveysResults);
    })
    .then((surveyCategoryData) => resolve(surveyCategoryData))
    .catch((error) => {
      logger.warn("Error occurred while fetching survey details", {error : error});
      return reject(error);
    })
  })
}


