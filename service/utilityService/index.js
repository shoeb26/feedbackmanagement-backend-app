
var log4js = require('log4js');
var logger = log4js.getLogger("utilityService");
var Q = require('q');
var extend = require('util')._extend;

exports.handleDBError = handleDBError;
/**
 * [handleDBError to handle error return from nano statusCode 400,403,406,404 is error and rest are warn]
 * @param  {[Error]} error  [Actual error return from nano ]
 * @param  {[Logger]} logger [This is logger object to print logs ]
 * @param  {[Data ]} data   [this will content data with message which you want top pass ]
 * @return {[type]}   [Not return any value]
 */

function handleDBError(error,logger,data) {
  data = data || {};
  logger.warn({data: data, error: error});
}