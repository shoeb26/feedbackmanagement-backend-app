var configurationService = require('nconf');
var logger = require('log4js').getLogger("nanoService");
var nano = require('nano');
var fs = require("fs");

module.exports = nanoFactory;

function adjustNano(nano) {

  var use = nano.use;
  nano.use = function(){
    var docModule = use.apply(nano, arguments);
    docModule.request = function(opts, callback) {
      if(!!callback){
        try{
          new Error('Callback should not be called as it leads to high memory consumption')
        }catch(e)
        {
          logger.warn("If you sent callback with get request it leads to memory leak", e.stack);
        }
      }
      opts.db = docModule.config.db;
      callback = callback || function(){};
      return nano.request(opts,callback);
    };
    return docModule;
  }

}

function nanoLogger(event) {
  logger.trace("Nano Event: ", event);
  if (!event.hasOwnProperty("err")) {
    logger.debug("Nano Start request: ", {
      request: {
        method: event.method,
        uri: event.uri
      }
    })
  } else {
    if (event.err) {
      logger.warn("Nano End badly, error: ", {error: event.err, uri: event.headers? event.headers.uri:"No header info available"})
    } else {
      var id = event.body && event.body._id || event.body && event.body.id;
      var rev = event.body && event.body._rev || event.body && event.body.rev;
      var status = event.body && event.body.ok;
      var code = event.headers && event.headers.statusCode;
      var date = event.headers && event.headers.date;
      var etag = event.headers && event.headers.etag;
      var contentType = event.headers && event.headers["content-type"];
      var type = event.body && event.body.type;
      var response = {};
      if(id) response.id = id;
      if(rev) response.rev = rev;
      if(status) response.status = status;
      if(code) response.code = code;
      if(date) response.date = date;
      if(type) response.type = type;
      if(!type && contentType) response.contentType = contentType;
      if(!rev && etag) response.etag = etag;
      logger.debug("Nano End response: ", {
        response: response
      })
    }
  }
}

function nanoFactory(configParameter) {
  var config = {};
  var databaseName;
  if(typeof configParameter == "string") {
    databaseName = configParameter
  } else if(configParameter) {
    databaseName = configParameter.databaseName;

    //Pass through parameters to nano
    config.log = configParameter.log;
    if(configParameter.requestDefaults) {
      config.requestDefaults = {};
      config.agent = configParameter.requestDefaults.agent
    }
  }
  config.url = configurationService.get("couchDBHostName") + ":" + configurationService.get("couchDBPort");
  if(configurationService.get('couchDB:enableTLS')) {
    config.requestDefaults = config.requestDefaults || {};
    config.requestDefaults.cert = fs.readFileSync(configurationService.get('couchDB:certificate'));
    config.requestDefaults.key = fs.readFileSync(configurationService.get('couchDB:privateKey'));
    config.requestDefaults.ca = fs.readFileSync(configurationService.get('couchDB:trustedCA'));
  }
  config.log = nanoLogger;
  if(databaseName) {
    let nanoR = nano(config);
    adjustNano(nanoR);
    return nanoR.use(databaseName)
  } else {
    let nanoR = nano(config);
    adjustNano(nanoR);
    return nanoR;
  }
}