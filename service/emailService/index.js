const logger = require('log4js').getLogger("emailService");
const configurationService = require("../configurationService");
const Q = require("q");


exports.sendEmail = sendEmail;


function sendEmail(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.toUser)
      throw new Error("To user not present");

    if(! opts.emailText)
      throw new Error("emailText not present");

    if(! opts.subject)
      throw new Error("subject not present");

    let fromUser =  configurationService.get("emailConfig:fromUser");
    let userCredentials = configurationService.get("emailConfig:credentials");

    var send = require("gmail-send")({
      user: userCredentials.user,
      // user: credentials.user,                  // Your GMail account used to send emails
      pass: userCredentials.password,
      // pass: credentials.pass,                  // Application-specific password
      to:   opts.toUser,
      // to:   credentials.user,                  // Send to yourself
                                               // you also may set array of recipients:
                                               // [ 'user1@gmail.com', 'user2@gmail.com' ]
      from:    fromUser+" "+fromUser,            // from: by default equals to user
      // replyTo: credentials.user,            // replyTo: by default undefined
      // bcc: 'some-user@mail.com',            // almost any option of `nodemailer` will be passed to it
      subject: opts.subject,
      //text:    test,         // Plain text
      html:    opts.emailText            // HTML
    });


    send((err, result) => {
      logger.debug("Sending emails result",err, result, opts);
    })
    return resolve()
  })
}