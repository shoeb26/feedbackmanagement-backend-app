'use strict';
/**
 * This file is used for configuration specific functions.
 */
var nconf = require('nconf');
var log4js = require('log4js');
var logger = log4js.getLogger("configurationService");
//Initialize some shims.
Object.entries || require('object.entries').shim();
Object.values || require('object.values').shim();

loadConfigParameters();

module.exports = nconf;
/**
 * Function used to load config file in nconf.
 */
function loadConfigParameters() {
  nconf.argv().env();
  if (process.env.VIENNA_CONFIG != undefined) {
    nconf.file('env', process.env.VIENNA_CONFIG)
  }
  nconf.file('default', __dirname + '/../../config/config-default.json');
}
