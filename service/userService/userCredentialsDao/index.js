const log4js = require('log4js');
const logger = log4js.getLogger("userCredentialsDao");
const configurationService = require('../../configurationService');
const db = require('../../nanoService')(configurationService.get('couchDB:couchDBUserCredentials'));
const utilityService = require('../../utilityService');
const moment = require('moment');
const Q = require('q');

exports.addCredentials = addCredentials;
exports.getUserCredetialsByUserName = getUserCredetialsByUserName;

function addCredentials(userCredentials){
	return Q.promise((resolve, reject) => {

    if(! userCredentials)
      throw new Error('User credentials missing');

    userCredentials.createdAt =  moment().toISOString();
    db.insert(userCredentials, function(err, body, header){
      if (err){
        utilityService.handleDBError(err,logger,{ message:"Error adding user in UserCredentials db "})
        return reject(err);
      }
      
      return resolve(body);      
    });
  })
}

function getUserCredetialsByUserName(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.userName)
      throw new Error("Username not defined");


    db.view('findByUserName','userName',{key : opts.userName}, (error, data) => {

      if(error){
        logger.warn("getUserCredetialsByUserName - Error occured while fetching user credentials",{error : error, 
          userName : opts.userName});
        return reject(error);
      }

      if(! data.rows.length){
        logger.warn("getUserCredetialsByUserName - Empty record for fetching user credentials",{userName : opts.userName});
        return reject({success: false ,msg : "User credentials not found"}); 
      }


      return resolve(data.rows[0].value);
    })
  })
}