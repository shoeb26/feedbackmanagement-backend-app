const log4js = require('log4js');
const logger = log4js.getLogger("userServiceDao");
const configurationService = require('../../configurationService');
const db = require('../../nanoService')(configurationService.get('couchDB:couchDBUsers'));
const utilityService = require('../../utilityService');
const moment = require('moment');
const Q = require("q");



exports.getUserByUserName =getUserByUserName;
exports.addUser =addUser;
exports.getUserByUserId = getUserByUserId ;
exports.getAllUsersByType = getAllUsersByType;


function getUserByUserName(userName){
  return Q.promise((resolve, reject) => {

    if(! userName)
      throw new Error("User name not present");

    db.view('findBy','userName',{key : userName, include_docs: true},(error , data) => {
      if (error) {
          utilityService.handleDBError(error,logger,{message: 'could not fetched user details',userName: userName});
          return reject(error);
      } else {

        if (data.rows.length > 0 && typeof data.rows[0].doc !== 'undefined')  {   
            logger.trace("fetched user details by userName from db ",{userName: userName, user :data.rows[0].doc});
            return resolve(data.rows[0].doc);
        } else {
            logger.warn('user not found for userName',{userName :userName, error:error});
            return reject({success : false, msg :  "User not present"});
        }
      }
    })
  })
}


function addUser(user){
  return Q.promise((resolve, reject) => {
    user.createdat = moment().toISOString();
    db.insert(user, function(err, body, header){
      if (err) {
        utilityService.handleDBError(err,logger,{ message:"Error inserting user "});
        return reject(err);
      }
      
      return resolve(body);
    });
  })
}

function getUserByUserId(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.userId)
      throw new Error("User id not present");


    db.get(opts.userId, function(err, body, header){
      if (err) {
        utilityService.handleDBError(err,logger,{ message:"Error inserting user "});
        return reject(err);
      }
      
      return resolve(body);
    });
  })
}

function getAllUsersByType(userType){
  return Q.promise((resolve, reject) => {

    let key = {};
    
    if(! userType)
      throw new Error("User type not present");

    if(userType != "all"){
      key.key = userType
    }

    key.include_docs =true

    db.view('findBy','userType',key,(error , data) => {
      if (error) {
          utilityService.handleDBError(error,logger,{message: 'could not fetched user details',userType: userType});
          return reject(error);
      } else {

        if (data.rows.length > 0 && typeof data.rows[0].doc !== 'undefined')  {   
            logger.trace("fetched user details by userType from db ",{userType: userType, user :data.rows[0].doc});
            return resolve(data.rows);
        } else {
            logger.warn('user not found for userType',{userType :userType, error:error});
            return reject({success : false, msg :  "User not present"});
        }
      }
    })
  })
}