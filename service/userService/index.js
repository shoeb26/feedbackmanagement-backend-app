const logger = require('log4js').getLogger("userService");
const configurationService = require("../configurationService");
const Q = require("q");
const moment  = require("moment");
const AuthorizationService = require("../authorizationService");
const authorizationService = new AuthorizationService();
const userDao = require("./dao");
const userStatus = configurationService.get('userStatus');
const userCredentialsDao = require('./userCredentialsDao');
const encryptionService = require("../encryptionService");
const defaultSessionLength =  configurationService.get("defaultSessionLength");
const crypto = require("crypto");
const emailService = require("../emailService");
const userTypes = ["volunteer", "Admin", "registeredUser"];
const _ = require("lodash");
const adminEmail = configurationService.get("emailConfig:adminEmail");

exports.login = login;
exports.register = register;
exports.getUserByUserId = userDao.getUserByUserId;
exports.forgotPassword = forgotPassword;
exports.saveUserToken = saveUserToken;
exports.getAllUsersByType = getAllUsersByType;
exports.changePassword = changePassword;
exports.sendSuggestions = sendSuggestions;
exports.updateUser = updateUser;

function login(opts){
	return Q.promise((resolve, reject) => {

    if(! opts.userName)
      throw new Error("User name not present");

    if(! opts.password)
      throw new Error("Password not present");

    logger.debug("Starting user login");
    let userData = userDao.getUserByUserName(opts.userName);
    let userCredentials = userCredentialsDao.getUserCredetialsByUserName({userName : opts.userName});

    Q.all([userData, userCredentials])
      .then((userData) => checkUserStatusAndPasswordVerification({password : opts.password, userData : userData}))
      .then((userData) => {
        let userSession = authorizationService.getAuthPayload({id : 1},userData[0],defaultSessionLength);
        return resolve({success : true, authPayload : userSession, userDetails : userData[0]});
      })
      .catch((error) => {
        logger.warn("Error occured while validating user",{error : error});
        return reject({success : false , error : error});
      })
  })
} 

function checkUserStatusAndPasswordVerification(opts){
  return Q.promise((resolve , reject) => {
    logger.debug("checkUserStatusAndPasswordVerification started");
    //check if user is active user or pending user
    /*if(! opts.userData[0].userStatus || opts.userData[0].userStatus == userStatus.pendingVerification){
      logger.warn("checkUserStatusAndPasswordVerification() -  User not verfied",{user : opts.userData[0]});
      return reject({success : false , msg :'User not active'});
    }*/

    //check if user password matches
    encryptionService.verifyPassword(opts.userData[1].password,opts.password,(error, result) => {
      if(error)  {
          logger.warn("checkUserStatusAndPasswordVerification - Error occured while verifying password")
          return reject({success : false , msg :'User not active', error : error});
      }

      // password mismatch
      if (!result) {
          logger.warn("checkUserStatusAndPasswordVerification - Password mismatch",{userName : opts.userData[0].userName});
          return reject({success : false , msg :'Password mismatch'});
      }

      return resolve(opts.userData);
    })
  })
}


function sendSuggestions(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.suggestions)
      throw new Error("suggestions not found");

    if(! opts.userEmail)
      throw new Error("user email not found");

    let emailText = "<p>Hello Admin,</p><p> Following are the suggestions from app users</p><p>Suggestion name : "+opts.suggestions.suggestionName+"</p>"+
    "<p>Suggestion body :</p><p>"+opts.suggestions.suggestionText+"</p><p>From user : "+opts.userEmail+"</p><p>Regards,<br> Saadmanuskichi Team</p>";
     emailService.sendEmail({toUser : adminEmail, subject : "App User Suggestions", emailText : emailText});
     return resolve()
  })
}


function getAllUsersByType(opts){
  return Q. promise((resolve, reject) => {

    if(! opts.userType)
      throw new Error("User type not defined");

    if(! opts.userId)
      throw new Error("User id not defined");


    if(opts.userType == "all"){
      userDao.getUserByUserId({userId : opts.userId})
      .then((userData) => {
        if(userData.userType == "Admin"){
          return userDao.getAllUsersByType("all");
        } else{
          return reject("all can be only fetched by admin");
        }
      })
      .then((userDetails) => userDetails.filter((user) => user.doc.userType != "Admin"))
      .then(resolve)
      .catch(reject)

    } else {
      userDao.getAllUsersByType(opts.userType)
      .then(resolve)
      .cath(reject)
    }


  })
}

function register(opts){
  return Q. promise((resolve, reject) => {

    if(! opts.userData)
      throw new Error("User data not defined");

    if(! opts.userName)
      throw new Error("User name not defined");

    let userData = opts.userData;
    userData.userName = opts.userName;
    userDao.getUserByUserName(opts.userName)
    .then((userData) =>  reject({success : false,msg :"User already present"}))
    .catch((error) => {
      if(error.msg == "User not present"){
        return Q.resolve();
      }
      return reject({success : false,error : error});
    })
    .then(() => addUserAndUserData({userData : userData}))
    .then(resolve)
    .catch((error) => {
      logger.warn("register() - Error occured while registering user",{error : error, userName : userData.userName});
      return reject({success : false , error : error});
    })
  })
}

function addUserAndUserData(opts){
  return Q.promise((resolve, reject) => {
    logger.trace("addUserandUserData called");
    let userData = opts.userData;
    let registeredDate = moment().toISOString();
    cleanUserDataForRegisteration(userData)
      .then((userData) => {
        let userDataToBeStored = _.cloneDeep(userData);
        userDataToBeStored.registeredDate =  registeredDate;

        if(userData.userType == "Admin"){
          userDataToBeStored.userStatus = userStatus.verified;
        } else {
          userDataToBeStored.userStatus = userStatus.pendingVerification;
        }
        delete userDataToBeStored.password;
        return userDao.addUser(userDataToBeStored);
      })  
      .then((user) => {

        let userPassData = {
          password : userData.password,
          userName : userData.userName,
          userId : user.id,
          registeredDate : registeredDate
        }

        return userCredentialsDao.addCredentials(userPassData);
      })
      .then(() => {
        if(userData.userType == "registeredUser"){
          let registeredUserText = "<p>Hello User</p><p>Welcome to Saadmanuskichi.At present your account is inactive and will be activated once reviewed by Administrator.</p><p>Till then you can login to app to check our other features.<p><p>Thank you for your patience.</p><p>Regards,<br> Saadmanuskichi Team</p>";
          return emailService.sendEmail({toUser : userData.userName, subject : "Welcome to Saadmanuskichi", emailText : registeredUserText});
        }

        return Q.resolve();
      })
      .then(resolve)
      .catch((error) => {
        logger.warn("Error occured while registering user",{error : error, userName : userData.userName});
        return reject({success : false , error : error});
      })
  })
}

function checkValidUserType(userType){

  if(! userType){
    throw new Error("User type not defined");
  }

  if(userTypes.indexOf(userType) == -1){
    throw new Error("Invalid user type");
  }

  return true;
}


function cleanUserDataForRegisteration(userData){
  return Q.promise((resolve , reject) => {
    checkValidUserType(userData.userType);

    if(userData.userType == "volunteer"){
      userData.userStatus = "active";
      userData.password = crypto.randomBytes(4).toString('hex');
    }

    if(userData.userType == "registeredUser" && ! userData.password){ 
      throw new Error("User password not defined");
    }

    encryptionService.getEncryptedValue(userData.password,(error, result) => {
      if(error){
        return reject(error);
      }

      userData.password = result;
      return resolve(userData);
    })
  })
}


function changePassword(userId, userPassowrd){
  return Q.promise((resolve, reject) => {

    if(! userId)
      throw new Error("userId not defined");

    if(! userPassowrd)
      throw new Error("userPassowrd not defined");


    let userCredentialsDb, userPasswordDb,userDetailsDb ;

      userDao.getUserByUserId({userId: userId})
      .then((userDetails) => {
        
        userDetailsDb =  userDetails;
        return userCredentialsDao.getUserCredetialsByUserName({userName : userDetails.userName});
      })
      .then((userDetailsCred) => {
        userCredentialsDb = userDetailsCred;
        return Q.nfcall(encryptionService.getEncryptedValue,userPassowrd)
      })
      .then((encryptedpass) => {
        userCredentialsDb.password =  encryptedpass;
        return userCredentialsDao.addCredentials(userCredentialsDb);
      })
      .then(resolve)
      .catch((error) => {
        logger.warn("Error occured while sending email",{error : error, userId : userId})
        return reject(error);
      })
    })
}


function saveUserToken(opts){
  return Q.promise((resolve, reject) => {
    
    if(! opts.userId)
      throw new Error("Username not defined");

    if(! opts.appToken)
      throw new Error("Toen not defined")

    userDao.getUserByUserId({userId: opts.userId})
    .then((userData) => {
      userData.appToken = {};
      userData.appToken = appToken;
      return userData;
    })
    .then((userData) => {
      userData.updatedAt =  moment().toISOString();
      return userDao.addUser(userData);
    })
    .then(resolve)
    .catch((error) => {
      logger.warn("Error occured while updating user details",{error: error, userId : opts.userId})
      return reject(error);
    })

  })
}
function forgotPassword(userName){
  return Q.promise((resolve, reject) => {

    if(! userName)
      throw new Error("Username not defined");

   let userData = userDao.getUserByUserName(userName);
   let userCredentials = userCredentialsDao.getUserCredetialsByUserName({userName : userName});

    let userCredentialsDb, userPasswordDb,userDetailsDb ;

    Q.all([userData, userCredentials])
      .then((userDetails) => {
        if(userDetails[0].userType == "Admin"){
          return reject("Cannot change password of admin");
        }

        userDetailsDb =  userDetails[0];
        userCredentialsDb = userDetails[1];
        userPasswordDb = crypto.randomBytes(4).toString('hex');
        return Q.nfcall(encryptionService.getEncryptedValue,userPasswordDb)
      })
      .then((encryptedpass) => {
        userCredentialsDb.password =  encryptedpass;
        return userCredentialsDao.addCredentials(userCredentialsDb);
      })
      .then(() => {
        let resetPasswordText = "<p>Hello User</p><p>Your updated passowrd is :"+userPasswordDb+"</p><p>Please use this passowrd to login to the app<p><p>Regards,<br> Saadmanuskichi Team</p>";
        return emailService.sendEmail({toUser : userDetailsDb.userName, subject : "Forgot Password", emailText : resetPasswordText});
      })
      .then(resolve)
      .catch((error) => {
        logger.warn("Error occured while sending email",{error : error, userName : userName})
        return reject(error);
      })
  })
}

function updateUser(opts){
  return Q.promise((resolve, reject) => {
    if(! opts.userId)
      throw new Error("UserId not defined");

    if(! opts.userData.userId)
      throw new Error("User id of edited user not defined")

    let activateUser = false,userEmail;

    if(opts.userData.userStatus == userStatus.verified){
      activateUser = true;
    }

    userDao.getUserByUserId({userId: opts.userData.userId})
      .then((userData) => {
        userEmail = userData.userName;
        let updatedUserData =  _.merge(userData, opts.userData);
        return userData;
      })
      .then((updatedUserData) => {
        updatedUserData.updatedAt =  moment().toISOString();
        updatedUserData.updatedBy = opts.userId;
        return userDao.addUser(updatedUserData);
      })
      .then((userData) => {
       console.log("activateUser",activateUser); 
        if(activateUser){
          let emailText = "<p>Hello User</p><p>Your account is now activated.</p><p>You can login check other restricted features of our app.<p><p>Regards,<br> Saadmanuskichi Team</p>";
          emailService.sendEmail({toUser : userEmail, subject : "Account Activated ", emailText : emailText});
        }
        return resolve();
      })
      .catch((error) => {
        logger.warn("Error occured while updating user",{userId : opts.userData.userId, error : error});
        return reject(error)
      })

  })
}
