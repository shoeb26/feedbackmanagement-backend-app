var bcrypt = require("bcrypt");
var log4js = require('log4js');
var logger = log4js.getLogger("encryptionAlgo");
let saltRounds = 10;

exports.getEncryptedValue = getEncryptedValue;
exports.verifyPassword = verifyPassword;

function getEncryptedValue(key,callback){
  try{

    bcrypt.hash(key, saltRounds, function(err, hash) {
      if(err){
        logger.error("Error while creating encrypted password: "+err);
        callback(err);
      } else {
        callback(null,hash);
      }
    })
  }catch(error){
    logger.error("Error while creating encrypted password: "+error);
    callback(error);
  }
}

function verifyPassword(encPassword,inputPassword,callback)
{
  if(typeof encPassword !== 'undefined' && typeof inputPassword !== 'undefined'){
    try{
      bcrypt.compare(inputPassword, encPassword, callback);
    } catch(error){
      logger.error("Error while verifying password: "+error);
    }
  } else {
    logger.error("Undefined password while verifying the password");
  }
}