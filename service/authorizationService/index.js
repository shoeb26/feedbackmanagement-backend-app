'use strict';

let crypto = require('crypto');
let jwt = require('jsonwebtoken');
let logger = require('log4js').getLogger("jwtAuthService");
let nconf = require('nconf');

let masterKey = nconf.get('authPayload:masterKey');
let defaultSessionTimeout = nconf.get('defaultSessionTimeout'); // now in Seconds
// key Rotation Schedule will be in seconds
let keyRotationSchedule = nconf.get('authKeyRotationSchedule'); // we wil change to keyRotationSchedule config
let algorithm =  "HS256"; // later we will do nconf.get('authPayload:algorithm');


function authoriztionService(){}

authoriztionService.prototype.getAuthPayload = createAuthToken;
authoriztionService.prototype.checkAuthenticationOfPayLoad = validateJWTToken;
authoriztionService.prototype.authorizeRequest = authorizeRequest;




module.exports = authoriztionService;


function createAuthToken(orgObj, userObj, sessionDurationMS, spooferUserObj, spooferOrgObj){

  let sessionDurationS = Math.floor(sessionDurationMS/1000) || defaultSessionTimeout;
  let jwtOptions = createJWTOptions(orgObj,userObj,sessionDurationS, encodeKeyId(generateKeyId()));
  let jwtPayload = createJWTPayload(orgObj,userObj,spooferUserObj, spooferOrgObj);
  let secretKey = generateSecretKey(decodeKeyId(jwtOptions.keyid));
  // let us now sign the JWT
  let signed = jwt.sign(jwtPayload,secretKey, jwtOptions);
  logger.trace('JWT',signed);
  return signed;
}


function validateJWTToken(token){

  let verified;
  // Since the frontend calls stringify on existing aoth payload we will get extra ""
  token = token.replace(/^\"/,'').replace(/\"$/,'');
  let decoded = jwt.decode(token,{complete: true});
  logger.trace('Decoded Token', decoded);

  // Let us validate if the library is able to decode
  // It is fail to decode if we have tampered with any item and return null
  if (decoded == null)
  {
    logger.warn("Unable to decode JWT Token", token);
    return false;
  }

  // let us check the algo
  if (decoded.header.alg !== algorithm){
    logger.warn("Mismatch Algorithm ", {expected: algorithm, actual: decoded.header.alg, token: token});
    return false;
  }

  // let us now verify using the keyid in the header
  let keyId = decodeKeyId(decoded.header.kid);
  // let us first validate if the keyId is correct
  // the keyId X keyRotationSchedule should be less than equal to the nbf
  let indexTimeStamp = keyId*keyRotationSchedule;

  if ( indexTimeStamp > decoded.payload.nbf || (indexTimeStamp <= decoded.payload.nbf - keyRotationSchedule))
  { // nbf in seconds
    logger.warn("validateJWTToken:keyId check failed", {keyId: keyId, nbf: decoded.payload.nbf})
    return false;
  }

  // let us check if the session duration exceeds the max allowed (defaultSessionDuration)
  let sessionDuration = decoded.payload.exp - decoded.payload.nbf;
  if (sessionDuration  > defaultSessionTimeout){
    logger.warn("validateJWTToken:sessionDuration check failed", {sessionDuration : sessionDuration});
    return false; 
  }
  
  let secretKey = generateSecretKey(keyId);

  try{
    verified = jwt.verify(token, secretKey);
  }
  catch(err){
    logger.warn('JWT Verify failed ', err.message);
  }
  return verified || false;
}



// NSJ : DRY Check with above function
function authorizeRequest(userId, orgId, token){
  // This is legacy authorize request
  // We will do what thelegacy authorizeRequest does
  let verified;
  token = token.replace(/^\"/,'').replace(/\"$/,'');
  let decoded = jwt.decode(token,{complete: true});
  logger.trace('Decoded Token', decoded);

  if (decoded == null)
  {
    return false;
  }

  // let us check the algo
  if (decoded.header.alg !== algorithm){
    return false;
  }

  // let us now verify using the keyid in the header
  let keyId = decodeKeyId(decoded.header.kid);
  // let us first validate if the keyId is correct
  // the keyId X keyRotationSchedule should be less than equal to the nbf
  let indexTimeStamp = keyId*keyRotationSchedule;

  if ( indexTimeStamp > decoded.payload.nbf || (indexTimeStamp <= decoded.payload.nbf - keyRotationSchedule))
  { // nbf in seconds
    logger.warn("validateJWTToken:keyId check failed", {keyId: keyId, nbf: decoded.payload.nbf})
    return false;
  }

  // let us check if the session duration exceeds the max allowed (defaultSessionDuration)
  let sessionDuration = decoded.payload.exp - decoded.payload.nbf;
  if (sessionDuration  > defaultSessionTimeout){
    logger.warn("validateJWTToken:sessionDuration check failed", {sessionDuration : sessionDuration});
    return false; 
  }

  let secretKey = generateSecretKey(keyId);
  let verifyOptions = {
    issuer: orgId,
    subject: userId,
    algorithms: [algorithm]
  }

  try{
    verified = jwt.verify(token, secretKey, verifyOptions);
  }
  catch(err){
    logger.warn('JWT Verify failed ', err.message);
  }


  return verified;

}

/**
* This function will authorize the request against the resource
* The resurce will be a string. (When we access through http , it will be of the form
* METHOD:URL ..) Currently, we will only support http resource names
*/
// July 4, 2017 : NSJ: We will just return the current authorizeRequest for now
function authorize(userId, orgId, token, resource){
  return authorizeRequest(userId,orgId,token);
}


/******* PRIVATE FUNCTIONS *******/

function createJWTOptions(orgObj, userObj, sessionDurationS, encodedKeyId){
  let jwtOptions = {
    algorithm: algorithm,
    expiresIn: sessionDurationS,
    notBefore: 0, // this token should not be used before the creation
    keyid: encodedKeyId,
    subject: userObj._id.toString(),
    issuer: orgObj.id.toString(),
    jwtid: crypto.randomBytes(8).toString('base64')
  }
  logger.trace('JWT Options', jwtOptions);
  return jwtOptions;
}

function generateKeyId(){
  let timeNow = Math.floor((new Date()).getTime() / 1000); // here we need the time in seconds
  let keyId = Math.floor(timeNow / keyRotationSchedule)
  return keyId;
}

function generateKeyIdBySchedule(rotationSchedule){
  let timeNow = Math.floor((new Date()).getTime() / 1000); // here we need the time in seconds
  let keyId = Math.floor(timeNow / rotationSchedule)
  return keyId;
}

function encodeKeyId(keyId){
  // keyId is an integer.
  // To encode it in base 64, we will need to convert to an hex and then read that hex into
  /// a buffer and output the buffer as base64
  // we also need to replave the training '=' if any
  let keyIdHex = keyId.toString(16);
  // We need to pad with 0 as the Buffer.from does not like unpadded hex string
  keyIdHex = (keyIdHex.length % 2)? '0'+keyIdHex:keyIdHex; 
  return Buffer.from(keyIdHex,'hex').toString('base64').replace(/=/g,'');
}

function decodeKeyId(encodedKeyId){
  return parseInt(Buffer.from(encodedKeyId,'base64').toString('hex'), 16);
}

function createJWTPayload(orgObj,userObj,spooferUserObj, spooferOrgObj){
  let jwtPayload = {
    uemail: userObj.email,
    otitle: orgObj.title
  };

  logger.trace('JWT Payload', jwtPayload);
  return jwtPayload;
}

function generateSecretKey(keyId){
  let secretHmac = crypto.createHmac('sha256',masterKey);
  let secretHmacKey = secretHmac.update(keyId.toString()).digest('base64');
  return secretHmacKey;
}

function generateSecretKeyByKey(masterKey, keyId){
  let secretHmac = crypto.createHmac('sha256',masterKey);
  let secretHmacKey = secretHmac.update(keyId.toString()).digest('base64');
  return secretHmacKey;
}

