const logger = require('log4js').getLogger("userService");
const configurationService = require("../configurationService");
const Q = require("q");
const moment  = require("moment");
const blogsDao = require('./dao');
const fs = require('fs');

exports.saveBlog = saveBlog;
exports.getBlogsList = blogsDao.getBlogsList;
exports.getBlogByBlogId = blogsDao.getBlogByBlogId;
exports.getReadStream = blogsDao.getReadStream;
exports.deleteBlog =  deleteBlog;

function saveBlog(opts){
	return Q.promise((resolve, reject) => {
    if(! opts.blogDetails)
      throw new Error("Blog details missing");

    if(! opts.userId)
      throw new Error("UserId is missing");

    let blogDetails = {
      userId : opts.userId,
      blogName : opts.blogDetails.blogName,
      blogDesc : opts.blogDetails.blogDesc,
      userType : opts.blogDetails.userType
    };

    let savedBlog;

    blogsDao.createBlog({blogDetails : blogDetails})
      .then((savedBlogDb) => {
        savedBlog = savedBlogDb;
        return readFileFromInputs({blogFile : opts.blogFile})
      })
      .then((fileData) => blogsDao.insertAttachment({id : savedBlog.id,rev : savedBlog.rev,
        fileName : opts.blogFile.name, data : fileData,dataType : opts.blogFile.type}))
      .then(() => {
        removeFileFromTemp(opts.blogFile.path,opts.blogFile.Name,new Date());
        return resolve();
      })
      .catch((error) => {
        logger.warn("Error occured while creating blogs",{error : error});
        return reject(error);
      })
  })
}

function deleteBlog(opts){
  return Q.promise((resolve, reject) => {
    if(! opts.userId)
        throw new Error("UserId is missing");

    if(! opts.blogId)
        throw new Error("UserId is missing");

    blogsDao.getBlogByBlogId({blogId : opts.blogId})
    .then((blog) => {
      if(!! blog._attachments)
        return blogsDao.destroyAttachament({id : blog._id,rev : blog._rev,attachmentName : Object.keys(blog._attachments)[0]});
      
      return true;
    })
    .then(() => {
      return blogsDao.getBlogByBlogId({blogId : opts.blogId});
    })
    .then((blog) => {
      return blogsDao.deleteBlog({ blogDetails : blog})
    })
    .then(resolve)
    .catch(reject)
  })  
}


function readFileFromInputs(opts){
    if(! opts.blogFile)
      throw new Error("File path not defined");

    return  Q.nfcall(fs.readFile,opts.blogFile.path);

}


function removeFileFromTemp(path,fileName,uploadTime){
  try{
    logger.debug("Deleted a file from path "+path+", actual file Name is "+fileName+" @ "+uploadTime);
    fs.unlinkSync(path);
  }catch(e){
    logger.warn("Error while deleting a file from path " +path+ ", actual file Name is "+fileName+" @ "+uploadTime);
    logger.warn("ApplicationHandler:removeFileFromTemp ",{error : e.stack} );
  } 
}