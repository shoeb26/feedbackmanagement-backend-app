const log4js = require('log4js');
const logger = log4js.getLogger("categoryDao");
const configurationService = require('../../configurationService');
const db = require('../../nanoService')(configurationService.get('couchDB:couchDBBlogs'));
const utilityService = require('../../utilityService');
const moment = require('moment');
const Q = require('q');

let limitRecords = 2000;

exports.createBlog = createBlog;
exports.getBlogByBlogId = getBlogByBlogId;
exports.insertAttachment = insertAttachment; 
exports.getBlogsList = getBlogsList;
exports.getReadStream = getReadStream;
exports.deleteBlog =deleteBlog;
exports.destroyAttachament = destroyAttachament;

function createBlog(opts){
	return Q.promise((resolve, reject) => {

    if(! opts.blogDetails)
      throw new Error("Blog details not defined");

    db.insert(opts.blogDetails,(err, data) => {
      if(err){
        logger.warn("Error occured while inserting the blog details");
        return reject(err);
      }

      return resolve(data);
    })
  })
}

function deleteBlog(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.blogDetails)
      throw new Error("Blog details not defined");

    opts.blogDetails._deleted = true;
    db.insert(opts.blogDetails,(err, data) => {
      if(err){
        logger.warn("Error occured while inserting the blog details");
        return reject(err);
      }

      return resolve(data);
    })
  })
}

function insertAttachment(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.id)
      throw new Error("FileData not found");

    db.attachment.insert(opts.id, opts.fileName, new Buffer(opts.data, "binary"), opts.dataType, {rev: opts.rev}, function (err, body) {
      if (err) {
        return reject(err);
      } else {
        return resolve(body);
      }
    });
  })
}


function getBlogByBlogId(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.blogId)
      throw new Error("BlogId not defined");

    db.get(opts.blogId, (error, body) => {
      if(error)
        return reject(error);

      return resolve(body);
    })
  })
}


function getBlogsList(opts){
	return Q.promise((resolve, reject) => {
    if(! opts.endIndex)
      throw new Error("End index not defined");

    if(! opts.userId)
      throw new Error("UserId not defined");
 

    db.view('findBy','userId',{include_docs : true,
     limit : limitRecords, skip : opts.endIndex},(err, data) => {
      if(err){
        logger.warn("Error occured while fetching user records",{error : err});
        return reject(err);
      }

      if (data.rows.length) {
        return resolve(data.rows);
      } else {
        return reject({success : false , msg : "No rows found"});
      }
    })
  })
}

function destroyAttachament(opts){
  return Q.promise((resolve, reject) => {
    db.attachment.destroy(opts.id, opts.attachmentName, {rev: opts.rev}, function (err, bodyOnDestroy) {
      if (err) {
        return reject(err);
      } else {
        return resolve();
      }
    });
 })
}

function getReadStream(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.docId)
      throw new Error("docId not defined");

    var readStream =  db.attachment.get(opts.docId, encodeURIComponent(opts.fileName), function (err, imageData) {
      if (err) {
          logger.warn('image stream failed to couchdb retrieved #2 for  ' + opts.fileName);
          return logger.warn('Error in urlImage : ',err);
      } else {
          logger.debug('image stream has been successfully attached at app UI ');
      }
    });

    return resolve(readStream);
  })
}