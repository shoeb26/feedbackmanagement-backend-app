const moment  = require("moment");
const logger = require('log4js').getLogger("surveyService");
const villageDao = require('./dao');
const configurationService  = require('../configurationService');
const userService = require('../userService');
const Q = require('q');
const adminOrgId =  configurationService.get('adminOrgId');

exports.saveDocs = villageDao.saveDocs;
exports.getVillageByDistrictandStateName = villageDao.getVillageByDistrictandStateName;
exports.getDistrictListByStateName = villageDao.getDistrictListByStateName;
