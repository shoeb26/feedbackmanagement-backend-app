const log4js = require('log4js');
const logger = log4js.getLogger("villageDao");
const configurationService = require('../../configurationService');
const db = require('../../nanoService')(configurationService.get('couchDB:couchDBVillageList'));
const utilityService = require('../../utilityService');
const moment = require('moment');
const Q = require('q');


exports.saveDocs = saveDoc;
exports.getVillageByDistrictandStateName = getVillageByDistrictandStateName;
exports.getDistrictListByStateName = getDistrictListByStateName;


function saveDoc(opts){
	 return Q.promise((resolve, reject) => {

    if(! opts.docToSave)
      throw new Error('Doc Details missing');

    if(opts.docToSave._id){
      opts.docToSave.updatedAt = moment().toISOString();
    } else {
      opts.docToSave.createdAt = moment().toISOString();
    }

    db.insert(opts.docToSave, function(err, body, header){
      if (err) {
        utilityService.handleDBError(err,logger,{ message:"Error inserting survey "});
        return reject(err);
      }
      return resolve(body);
    });
  })
}


function getVillageByDistrictandStateName(opts){
  return Q.promise((resolve, reject) => {

    if(! opts.districtName)
      throw new Error("districtName not present");

    if(! opts.stateName)
      throw new Error("stateName not present");

    let key = [opts.stateName, opts.districtName];

    db.view('findBy','districtAndState',{key : key, include_docs: true},(error , data) => {
      if (error) {
          utilityService.handleDBError(error,logger,{message: 'could not fetched user details',districtName: opts.districtName, 
            stateName : opts.stateName});
          return reject(error);
      } else {

        if (data.rows.length > 0)  {   
            logger.trace("fetched village details by district and statename from db ",{districtName: opts.districtName, stateName :opts.stateName});
            return resolve(data.rows);
        } else {
            logger.warn('village details by district and statename from db not found ',{districtName: opts.districtName, stateName :opts.stateName, error:error});
            return reject({success : false, msg :  "village not present"});
        }
      }
    })
  })
}

function getDistrictListByStateName(opts){
  return Q.promise((resolve, reject) => {


    if(! opts.stateName)
      throw new Error("stateName not present");


    db.view('findBy','stateName',{key : opts.stateName, include_docs: true},(error , data) => {
      if (error) {
          utilityService.handleDBError(error,logger,{message: 'could not fetched distict details',districtName: opts.districtName, 
            stateName : opts.stateName});
          return reject(error);
      } else {

        if (data.rows.length > 0)  {   
            logger.trace("fetched village details by statename from db ",{stateName :opts.stateName});
            return resolve(data.rows);
        } else {
            logger.warn('village details by statename from db not found ',{stateName :opts.stateName, error:error});
            return reject({success : false, msg :  "village not present"});
        }
      }
    })
  })
}