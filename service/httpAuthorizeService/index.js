/**
 * http Authorizer. This service will check if URL needs authorization or not. 
 * If it needs authorization then authorizationService will be called, 
 * if it returns true the api will be hit and if it returns false
 * then 401 will be sent in the response. 
 */
var AuthorizationService = require('../authorizationService');
var authorizationService = new AuthorizationService();
var log4js = require('log4js');
var logger = log4js.getLogger("httpAuthorizerService");
var nconf = require('nconf');


function httpAuthCheckJWT(req,res,next){  
    var permission = authorizationService.checkAuthenticationOfPayLoad(req.headers.authpayload); 
    if(!!permission){
        if(req.params.hasOwnProperty("user")){
            let verified = authorizationService.authorizeRequest(req.params.user.id,req.params.org.id,req.headers.authpayload);
            if((validateParams(req)) && !!verified){
                logger.debug("Authorized");
                next();
                return;
            } else {
                logger.warn('API Call ' + req.url + ' Forbidden');
                logger.trace(req.url + ' headers: ' + JSON.stringify(req.headers));
                res.send(401,{"success": false,"message":"Forbidden"});
            }
        }
        if(req.params.hasOwnProperty("device")){
            let verified = authorizationService.authorizeRequest(req.params.device.id,req.params.org.id,req.headers.authpayload);
            if((validateParams(req)) && !!verified){
                logger.debug("Authorized");
                next();
                return;
            } else {
                logger.warn('API Call ' + req.url + ' Forbidden');
                logger.trace(req.url + ' headers: ' + JSON.stringify(req.headers));
                res.send(401,{"success": false,"message":"Forbidden"});
            }
        }
    }
    else
    {
      logger.warn('API Call ' + req.url + ' not authorized');
      logger.trace(req.url + ' headers: ' + JSON.stringify(req.headers));
      res.send(401,{"success": false,"message":"Unauthorized"});
    } 
}

function httpAuthCheck(req,res,next){
    if(typeof req.headers.authpayload !== 'undefined'){
      var authpayloadStr = ((typeof req.headers.authpayload !== 'undefined') && (typeof req.headers.authpayload === 'object')) ? JSON.stringify(req.headers.authpayload) : req.headers.authpayload;
      httpAuthCheckJWT(req,res,next);
    }else{
      // If we reach here , we are invalid ... Let us send unauthorized  
      logger.warn('API Call ' + req.url + ' not authorized');
      logger.trace(req.url + ' headers: ' + JSON.stringify(req.headers));
      res.send(401,{"success": false,"message":"Unauthorized"});
    }
    
}

exports.httpAuthorizerCheck = httpAuthCheck;


function validateParams(req){
    if(req.params.hasOwnProperty('user')  && req.params.hasOwnProperty('org')){
        if(req.params.user.hasOwnProperty('id')  && req.params.org.hasOwnProperty('id')){
            return true;
        }
        else{
            return false;
        }
    }else if(req.params.hasOwnProperty('device') && req.params.hasOwnProperty('org')){
        if(req.params.device.hasOwnProperty('id') && req.params.org.hasOwnProperty('id')){
            return true;
        }
        else{
            return false;
        }
    }else{
        return false;
    }
}
