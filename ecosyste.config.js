module.exports = {
    /**
     * Application configuration section
     * http://pm2.keymetrics.io/docs/usage/application-declaration/

     */
    apps : [

        // Vienna application
        {
            name      : 'saadmanuski',
            script    : 'server.js',
            cwd       : '/home/saadmanuski',
            exec_mode : 'cluster',
            instances : 2,
            node_args : "--max_old_space_size=8192 --nouse-idle-notification --expose-gc --abort-on-uncaught-exception",
            //instance_var: 'INSTANCE_ID',
            //listen_timeout : 3000,
            //kill_timeout : 3000,
            //max_restarts : 20,
            env: {
                COMMON_VARIABLE: 'true'
            },
            env_production : {
                NODE_ENV: 'production'
            },
            //log_date_format: "YYYY-MM-DD HH:mm Z",
            error_file: "/var/log/saadmanuski/saadmanuski-stderr.log",
            out_file: "/var/log/saadmanuski/saadmanuski-stdout.log",
            //combine_logs: true,
            merge_logs: true,
            //pid_file: "/var/run/vienna.pid",
            max_memory_restart: "1G"
        }
    ]
};
