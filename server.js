var configurationService = require('./service/configurationService');
var authorizationService = require('./service/authorizationService');
//var logService = require('./service/logService');
var log4js = require('log4js'),
  restify = require('restify'),
  parameterize = require('./service/parameterizerService'),
  loggerService = require('./service/logService'),
  logger = require('log4js').getLogger("app")
/**
 * handler declaration
 * @type {exports}
 */
var userHandler = require('./routes/UserHandler'),
apiHandler = require('./routes/ApiHandler'),
surveyHandler = require('./routes/SurveyHandler'),
categoryHandler = require('./routes/CategoryHandler'),
villageHandler = require('./routes/VillageHandler'),
blogHandler = require('./routes/BlogHandler');


//Let us init the database
var couchInitService = require("./service/couchInitService");
couchInitService.init();

var server = restify.createServer({
  name: 'vienna'
});

server.use(restify.CORS());
server.opts(/.*/, function (req,res,next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", req.header("Access-Control-Request-Method"));
  res.header("Access-Control-Allow-Headers", req.header("Access-Control-Request-Headers"));
  res.send(200);
  return next();
});
//CORS
server.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,HEAD');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.header('Content-Type', 'application/json');
  next();
});

server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(parameterize.parameterize);


/**
 * Create handler obj and pass to ApiHandler
 * @type {{users: (*|exports), applications: (*|exports), autoRegistration: (*|exports), sysUpdate: (exports|*), defaultApp: (exports|*), deviceFunctionalities: (exports|*), apkHandler: (*|exports), dashboardHandler: (exports|*)}}
 */
var handlers = {
  users: userHandler,
  survey : surveyHandler,
  category : categoryHandler,
  blogs : blogHandler,
  village : villageHandler
};

server.listen(configurationService.get('restApiPort'), function () {
  apiHandler.setup(server, handlers);
  logger.info('Restapi server listening on port ' + configurationService.get('restApiPort') + ' in development mode');
});

