const xlsx = require('xlsx');
const logger = require('log4js').getLogger('importCategories');
const Q = require('q');
const villageService = require('../service/villageService');
const parseArgs = require('minimist');
let args =  parseArgs(process.argv);
let _ =  require('lodash');
const async  = require('async');
let village = {};
let finalCategoryObject = {};
let stateName = "Maharashtra";
let validLocales = ["mr", "en"];
const crypto = require('crypto');
let schools,districtDoc ={},villageDocs = [];
let schoolMap = new Map(); //key : distrtciName , value : villageNameObjects
let villageDistrictSet = new Set();
start();

console.log(villageService);
function start(){
  logger.info("Starting with reading excel file");



  readFile()
  .then(parseCategoryObjects)
  .then(createSaveDocuments)
  .then(saveDistricts)
  .then(saveVillages)
  .then(() => process.exit(0))
  .catch((error) => {
    console.log("error--------------------------",error);
  })

}


function readFile(){
  return Q.promise((resolve, reject) => {
    console.log("Starting with reading excel file")
    var workbook = xlsx.readFile(__dirname+'\/'+'school.xlsx');
    if(! workbook)
      throw new Error("Excel workbook not found");

    //lets first parse each and every category form and object and then asynchronusly write these objcets to db
    var jsonRepresentation = {};
    var ws = workbook.Sheets[Object.keys(workbook.Sheets)[0]];
    var jsonObj = xlsx.utils.sheet_to_json(ws,jsonRepresentation);
    schools = jsonObj;

    return resolve();
  })
}


function parseCategoryObjects(){
  return Q.promise((resolve, reject) => {

	
	schools.forEach((schoolObject) => {
		let details = {};
		if(!! schoolObject.District && !! schoolObject.villagename){
      let villName = schoolObject.District+schoolObject.villagename;
		  details.villageName = schoolObject.villagename[0].toUpperCase()+schoolObject.villagename.toLowerCase().slice(1);
		  details.villageCode = crypto.randomBytes(4).toString('hex');
		  let mapObj = schoolMap.get(schoolObject.District);
      if(! villageDistrictSet.has(villName)){
        if(! mapObj ){
          schoolMap.set(schoolObject.District,[details]); 
        } else{
          mapObj.push(details); 
        }
      }
      villageDistrictSet.add(villName);
		}
	});
	return resolve();
  })
}


function createSaveDocuments(){
	return Q.promise((resolve, reject) => {
		let districts = Array.from(schoolMap.keys()).map((district) => {
			let villageObject = schoolMap.get(district);
			if(!! villageObject){
				villageDocs.push({
					type : "village",
					districtName : district[0].toUpperCase()+district.toLowerCase().slice(1),
					stateName : stateName,
					villages : villageObject
				})
			}
			return {
				districtName : district[0].toUpperCase()+district.toLowerCase().slice(1)
			}
		})

		districtDoc.type ="district";
		districtDoc.stateName = stateName;
		districtDoc.districts = districts;
		return resolve()
	})
}
function saveDistricts(){
  return Q.promise((resolve, reject) => {
    villageService.saveDocs({docToSave : districtDoc})
    .then((data) => console.log("Distrcits saved succefully", data))
    .catch((error) => console.log("Error occured while saving category",error))
    .finally(function(){ 
      return resolve();
    })
  })
}


function saveVillages(){
  return Q.promise((resolve, reject) => {
    async.eachLimit(villageDocs,3,saveDoc, (err,data) => {
      if(err)
        return reject(err);
      return resolve();
    })
  })
}

function saveDoc(dataObject,callback){
  villageService.saveDocs({docToSave : dataObject})
    .then((data) => console.log("Vilages saved succefully", data))
    .catch((error) => console.log("Error occured while saving category",error))
    .finally(function(){ 
    	return callback(null,true);
    })
}