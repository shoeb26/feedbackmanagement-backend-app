const xlsx = require('xlsx');
const logger = require('log4js').getLogger('importCategories');
const Q = require('q');
const categoryService = require('../service/categoryService');
const parseArgs = require('minimist');
let args =  parseArgs(process.argv);
let _ =  require('lodash');
const async  = require('async');
let categoryObjects = {};
let finalCategoryObject = {};
let locale = args.locale;
let validLocales = ["mr", "en"];
start();

function start(){
	logger.info("Starting with reading excel file");

  if(! locale || validLocales.indexOf(locale) == -1)
    throw new Error("Please define valid locales");

  readFile()
  .then(parseCategoryObjects)
  .then(saveCategories)
  .then(() => process.exit(0))
  .catch((error) => {
    console.log("error--------------------------",error);
  })

}


function readFile(){
  return Q.promise((resolve, reject) => {
    console.log("Starting with reading excel file")
    var workbook = xlsx.readFile(__dirname+'\/'+'saadmanuskimr.xls');
    if(! workbook)
      throw new Error("Excel workbook not found");

    //lets first parse each and every category form and object and then asynchronusly write these objcets to db
    Object.keys(workbook.Sheets).forEach((category) => {
      var jsonRepresentation = {};
      var ws = workbook.Sheets[category];
      var jsonObj = xlsx.utils.sheet_to_json(ws,jsonRepresentation);
      categoryObjects[category] = jsonObj;
    });
    return resolve();
  })
}


function parseCategoryObjects(){
  return Q.promise((resolve, reject) => {

    var categoryCombined;
    Object.keys(categoryObjects).forEach((categoryName) => {
      let categoryObject = {
        categoryName : categoryName,
        subCategoryDetails : []
      };

      categoryCombined = categoryObjects[categoryName].reduce((acc, categoryObject) => {
        let subCategory = {};
        let details = {};

        if(!! categoryObject.Component){
          subCategory.categoryName = categoryObject.Component;
        }

        if(! categoryObject.Subclass && !! categoryObject.points && !! categoryObject.Component){
          details.name = categoryObject.Component
          details.points = categoryObject.points || 1;
          details.comments  = categoryObject.comments || "NA";
          subCategory.details = subCategory.details || [];
          subCategory.details.push(details); 
        }

        if(!! categoryObject.Subclass){
          subCategory = acc[acc.length -1];
          details.name = categoryObject.Subclass;
          details.points = categoryObject.points || 1;
          details.comments  = categoryObject.comments || "NA";
          subCategory.details = subCategory.details || [];
          subCategory.details.push(details); 
          acc.splice(acc.length -1,1);
        }

        acc.push(subCategory);
        return acc;
      },[]);

      finalCategoryObject[categoryName] = categoryCombined;
    })

    return resolve();
  })
}

function saveCategories(){
  return Q.promise((resolve, reject) => {
    async.eachLimit(Object.keys(finalCategoryObject),2,saveCategory, (err,data) => {
      if(err)
        return reject(err);
      return resolve();
    })
  })
}

function saveCategory(categoryName,callback){
  let category = finalCategoryObject[categoryName];
  let categoryDoc = {
  	categoryName : categoryName,
  	subCategoryDetails : category
  }

  categoryService.saveCategory({categoryDoc : categoryDoc, locale : locale})
    .then((data) => console.log("Categpry saved succefully", data))
    .catch((error) => console.log("Error occured while saving category",error))
    .finally(function(){ 
    	return callback(null,true);
    })
}