const logger = require('log4js').getLogger('createAdmin');
const Q = require('q');
const userService = require('../service/userService');
const parseArgs = require('minimist');
let args =  parseArgs(process.argv);

let userName = args.userName;
let password = args.password;

if(! userName || !password){
	throw new Error("userName or password not present");
}

let adminData ={
	villageName : "Pune",
	phoneNo : "977989690",
	password : password,
	userType : "Admin"
}


userService.register({userName : userName, userData : adminData})
.then((status) => {
	console.log("user added successfully")
})
.catch((error) => {
	console.log("error occured while creating user");
})
